# IDU3191 Project

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/9ca38740-90d2-4101-a9e3-0a380a6d5b3c/mini.png)](https://insight.sensiolabs.com/projects/9ca38740-90d2-4101-a9e3-0a380a6d5b3c)

This project is for subject IDU3191 (Meeskonnatöö infosüsteemi arendamisel). The full name of it is "Põlevkivi Kompetentsi keskuse koolitustele registreerimise allsüsteem".

## Description in Estonian

Infosüsteem Põlevkivi Kompetentsi keskuse koolitustel osalejate jaoks, kes vajavad koolitustele registreerumissüsteemi. Toode on veebirakenduse alusel töötav koolitustele registreerumissüsteem, mis võimaldaks kergeid registreerumisi.

Võrreldes praeguse süsteemiga, meie toode võimaldab mugavamat ja paindlikumat registreerumist interneti kaudu.

## Learning

Get overview of Symfony in 10 minutes on http://symfony.com/doc/current/quick_tour/the_big_picture.html

## Authors

Jelena Kuzmina _(132304IAPM)_  
Jevgeni Rumjantsev _(144252IAPM)_  
Marina Lavrentjeva _(143688IAPM)_  
Sergei Popov _(132554IAPM)_  
Viktor Popkov _(132458IAPM)_  
Vladimir Morozov _(143637IAPM)_
