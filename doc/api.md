# API

## /api/v1/courses ##

### `GET` /api/v1/courses.{_format} ###

_List all courses._

List all courses.

#### Requirements ####

**_format**



### `POST` /api/v1/courses.{_format} ###

_Create a new course._

Create a new course.

#### Requirements ####

**_format**


#### Parameters ####

course:

  * type: object (CourseType)
  * required: true

course[name]:

  * type: string
  * required: true
  * description: Name

course[description]:

  * type: string
  * required: true
  * description: Description

course[isCommentable]:

  * type: boolean
  * required: false
  * description: Allow commenting

course[start]:

  * type: date
  * required: false
  * description: Course starts

course[end]:

  * type: date
  * required: false
  * description: Course ends

course[registrationStart]:

  * type: date
  * required: false
  * description: Registration starts

course[registrationEnd]:

  * type: date
  * required: false
  * description: Registration ends

course[maxParticipants]:

  * type: float
  * required: true
  * description: Maximum participants

course[location]:

  * type: string
  * required: true
  * description: Location


### `DELETE` /api/v1/courses/cancel/{id} ###

_Cancel registration to course._

Cancel registration to course.

#### Requirements ####

**id**

  - Requirement: \d+
  - Type: int
  - Description: Course id


### `POST` /api/v1/courses/register/{id} ###

_Register to course._

Register to course.

#### Requirements ####

**id**

  - Requirement: \d+
  - Type: int
  - Description: Course id


### `GET` /api/v1/courses/{id}.{_format} ###

_Get a single course._

Get a single course.

#### Requirements ####

**id**

  - Requirement: \d+
  - Type: int
  - Description: course id
**_format**



### `DELETE` /api/v1/courses/{id}.{_format} ###

_Remove a course._

Remove a course.

#### Requirements ####

**id**

  - Requirement: \d+
  - Type: int
  - Description: Course id
**_format**



## /api/v1/feedback/templates ##

### `GET` /api/v1/feedback/templates.{_format} ###

_List all feedback templates._

List all feedback templates.

#### Requirements ####

**_format**



### `GET` /api/v1/feedback/templates/{id}.{_format} ###

_Get a single feedback template._

Get a single feedback template.

#### Requirements ####

**id**

  - Requirement: \d+
  - Type: int
  - Description: template id
**_format**



## /api/v1/users ##

### `GET` /api/v1/users.{_format} ###

_List all users._

List all users.

#### Requirements ####

**_format**



### `GET` /api/v1/users/me.{_format} ###

_Get a current logged in user._

Get a current logged in user.

#### Requirements ####

**_format**



### `GET` /api/v1/users/{id}.{_format} ###

_Get a single user._

Get a single user.

#### Requirements ####

**id**

  - Requirement: \d+
  - Type: int
  - Description: user id
**_format**
