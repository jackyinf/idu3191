# Known issues

## In frontend

#### 1. (Solved) Angular app files are loaded twice.

This is most likely due to use of `ng-app` and `angularAMD.bootstrap(app)`. The first one should be removed.

> **Solution:** Removed `ng-app`. Definition and application AMD bootstrapping is now in one file.

#### 2. (Solved) Incorrect definition of Angular RequireJS controllers-services.

Controllers and services should be used with `define` statement and return an object, which they represent.

> **Solution:** Now every angular component is defined using `define`.

#### 3. (Solved) Problems using 3rd party (Angular) modules like toastr.

3rd party module, named toastr, uses predefined partial views defined in <somename>.tpls.js (tpls - templates) and Symfony routing rules disallow calling those partial views.

> **Note 1:** There is a possibility, that those problems can occur when using some other 3rd party modules.

> **Note 2:** Angular templates can be defined synchronously using `<script type="text/ng-template" id="my_template_name.html">...template content....</script>` and called using "id".

> **Solution:** There is a config, which clears all the templates' cache. Toaster loaded its partials into cache and got removed. So toaster's partials are defined again after template cache removal.

#### 4. (Solved) Problem with Bootstrap Material Design.

There is a problem when loading any CSSS files (using @import in SCSS).

> **Solution:** in gruntfile, destination should be SCSS, where source file's type is CSS, in order to import styles successfully. If we import CSS into SCSS, then it will look for CSS in wrong directory.

## In backend

#### 1. Composer dependencies updating doesn't work

Composer requires a lot of memory for PHP in order to update packages (https://github.com/composer/composer/issues/1898), so keep that in mind if you planning to use you own development environment and give it at least 2Gb of RAM.

> The best solution is to update dependencies on your local machine instead of current project Vagrant box, because I didn't want to give so much memory to it.

#### 2. Missing services:

> Cancelling registration: `/api/v1/courses/cancel-registration`