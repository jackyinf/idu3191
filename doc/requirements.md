# Requirements

## Minimum

For development purposes the easiest way to run the project is using [Vagrant](https://www.vagrantup.com/) (make sure to take a look /doc/run_using_vagrant.md). Everything is preconfigured, but if you would like to use your own development environment take a look at /doc/run_manually.md:

 - PHP 5.3 with `php5-mysql` module
 - [Apache](http://www.apache.org/) or [Nginx](http://nginx.org/)
 - [MySQL](http://www.mysql.com/) _**(default)**_
 - [composer](https://getcomposer.org/)

## Optimal

 - PHP 5.3 or later with modules:
     + `php5-curl`
     + `php5-intl`
     + `php5-mysql`
     + `php5-mcrypt`
     + `php5-pgsql`
     + `php5-xdebug`
 - [Nginx](http://nginx.org/)
 - DB:
     + [MySQL](http://www.mysql.com/) _**(default)**_
     + [PostgreSQL](http://www.postgresql.org/) _(alternative)_
 - [composer](https://getcomposer.org/)
 - [npm](https://www.npmjs.com/) + [Grunt](http://gruntjs.com/) + [Bower](http://bower.io/)
