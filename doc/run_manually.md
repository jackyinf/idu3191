# Run manually

_If you have chosen this way to run a project, then I guess you already know what to do and no long instruction are required._

After you have configured you own server with all ther requirements just follow 3 easy steps:

**Step 1:** Install vendors with all the dependencies.

```bash
$ composer install
```

**Step 2:** Create schema.

```bash
$ app/console doctrine:schema:create
$ app/console doctrine:schema:create --em=local
```

**Step 3:** Install node and bower modules and run Grunt. This step is optional if you are not interested in the frontend but still is highly recommended.

```bash
$ npm install
$ bower install
$ grunt       # for development
$ grunt build # for production
```
