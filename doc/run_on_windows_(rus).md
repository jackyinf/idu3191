# Полная инструкция по настройке рабочей среды на Windows

## Скачай и поставь

 - Git http://git-scm.com/downloads
 - Vagrant https://www.vagrantup.com/downloads.html
 - PhpStorm (IDE для зарзаботки) https://www.jetbrains.com/phpstorm/
 - Putty (http://the.earth.li/~sgtatham/putty/latest/x86/putty.exe)
 - PuttyGen (http://the.earth.li/~sgtatham/putty/latest/x86/puttygen.exe)
 
Студентам PhpStorm бесплатный, надо иметь при себе ISIC карту и подать заявку тут: https://www.jetbrains.com/estore/students/

## Клонирование проекта

Нужно открыть какую-нибудь пустую папку, где будет в будущем храниться проект. Открыть там консоль и ввести:

```bash
git clone https://bitbucket.org/jackyinf/idu3191.git
```

Нужно будет ввести имя пользователя и пароль, которые используются на bitbucket.org.

Затем в коммандной строке надо набрать:

```bash
vagrant up
```

и подождать несколько минут.

Создается папка .vagrant. Открываем программу PuttyGen и нажимаем Load, затем идем по директории .vagrant\machines\default\virtualbox, ставим отображение всех типов файлов справа снизу (All files .*) и выбираем файл private_key. 

Затем ставим справа снизу число (Number of bits in generated key) на 2048 (если уже не стояло) и нажимаем "Save private key". Сохраняем новый файл под любым именем и в любую папку. Я сохраняю в ту-же папку под названием generated.ppkg.

Открываем putty.exe. Слева виден список категорий. В категории session (нажимаем), вводим:

 - Host Name: "127.0.0.1"
 - Port: 2222

В категории connection/data (нажимаем), вводим:

 - Auto-login username: "vagrant"

В категории connection/ssh/auth (нажимаем), вводим:

 - Private key file for authentication --> Нажимаем "Browse..." и находим нами сгенерированный с PuttyGen файл.
 
Под конец нажимаем "Open". Открывается новое консольное окно. Вводим следующие комманды:

```bash
cd ../../
cd vagrant/www/public_html   # навигация к приложению
composer install             # вроде необязательно... ждем, если запущена
php app/console server:run   # запуск приложения
```

И под конец открываем у себя в браузере [192.168.33.99/app_dev.php](http://192.168.33.99/app_dev.php). Должно появится красивенькое окошечко с приведеницем.
