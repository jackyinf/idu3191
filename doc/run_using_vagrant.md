# Run using Vagrant

_The Vagrant box for this project was intentionally configured to use up to 2GB of RAM. Check the "Known issues" section if you would like to know why…_

Just quick reminder:

```bash
$ vagrant up      # start
$ vagrant halt    # stop
$ vagrant destroy # delete
```

So in order to start the project simply run `vagrant up` and you are ready to go. Please check the "About Vagrant box" section to learn more.

If you have troubles running it on Windows make sure to check "Полная инструкция по настройке рабочей среды на Windows" section.

## About Vagrant box

The server is by default running [Nginx](http://nginx.org/) + [MySQL](http://www.mysql.com/). MySQL can be easily changed to PostgreSQL.

The project by itself is located in `/var/www/pkk/public_html`.

The logs can be found in `/var/www/pkk/logs`.

**MySQL _(default)_:**

```bash
127.0.0.1 # address
3306      # port
pkk_db    # database name
user      # username
password  # password
```

**PostgreSQL _(alternative)_:**

```bash
127.0.0.1 # address
5432      # port
pkk_db    # database name
user      # username
password  # password
```

**SSH:**

To access directly:

```bash
192.168.33.99 # address
22            # port
vagrant       # username
vagrant       # password
```

To access using forwarded agent:

```bash
127.0.0.1 # address
2222      # port
vagrant   # username
vagrant   # password
```

Either ways are valid.

### Change MySQL to PostgreSQL

Just uncomment `pgsql` and comment out `mysql` in /ansible/playbook.yml and recreate your box again.

Don't forget to change /www/public_html/app/config/parameters.yml with the settings provided above.
