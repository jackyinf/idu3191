/*!
 * Gruntfile.
 *
 * based on FireShell Gruntfile:
 * http://getfireshell.com
 *
 * @author     Todd Motto
 * @author     Victor Popkov <Dragon.vctr@gmail.com>
 * @modified   26.03.2015
 */

'use strict';

/**
 * Grunt module
 */
module.exports = function (grunt) {

    /**
     * Dynamically load npm tasks
     */
    require('load-grunt-tasks')(grunt);

    /**
     * Time how long tasks take. Can help when optimizing build times
     */
    require('time-grunt')(grunt);

    /**
     * FireShell Grunt config
     */
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        /**
         * Set project info
         */
        project: {
            port: 9000,
            livereload: 35729,
            app: 'web',
            src: '<%= project.app %>/src',
            assets: '<%= project.app %>/assets',
            files: {
                css: '<%= project.assets %>/css/style.css',
                css_min: '<%= project.assets %>/css/style.min.css',
                scss: '<%= project.src %>/scss/style.scss'
            }
        },

        /**
         * Deletes specified files or clean directories
         * https://github.com/gruntjs/grunt-contrib-clean
         */
        clean: {
            vendors: ['<%= project.src %>/scss/vendor/*', '<%= project.src %>/js/vendor/*', '!<%= project.src %>/scss/vendor/_*.scss'],
            assets: ['<%= project.assets %>/css/*', '<%= project.assets %>/js/main.min.js']
        },

        /**
         * https://github.com/timmywil/grunt-bowercopy
         * Copies specified files from `bower_components`
         */
        bowercopy: {
            bourbon: {
                options: {
                    destPrefix: '<%= project.src %>/scss/vendor'
                },
                files: {
                    'bourbon': 'bourbon/app/assets/stylesheets'
                }
            },
            foundation: {
                options: {
                    destPrefix: '<%= project.src %>'
                },
                files: {
                    // scss
                    'scss/vendor/foundation': 'foundation/scss/foundation',

                    // js
                    'js/vendor/modernizr.min.js': 'foundation/js/vendor/modernizr.js',
                    'js/vendor/jquery/jquery.min.js': 'foundation/js/vendor/jquery.js',
                    'js/vendor/jquery/jquery.cookie.min.js': 'foundation/js/vendor/jquery.cookie.js',
                    'js/vendor/jquery/jquery.placeholder.min.js': 'foundation/js/vendor/placeholder.js',
                    'js/vendor/fastclick.min.js': 'foundation/js/vendor/fastclick.js',
                    'js/vendor/foundation': 'foundation/js/foundation'
                }
            },
            bootstrap: {
                options: {
                    destPrefix: '<%= project.src %>'
                },
                files: {
                    // Bootstrap
                    'scss/vendor/bootstrap': 'bootstrap-sass-official/assets/stylesheets',
                    'js/vendor/bootstrap': 'bootstrap-sass-official/assets/javascripts',
                    'fonts/vendor/bootstrap': 'bootstrap-sass-official/assets/fonts/bootstrap',

                    // Angular Bootstrap
                    'js/vendor/angular/ui-bootstrap.js': 'angular-bootstrap/ui-bootstrap.js',
                    'js/vendor/angular/ui-bootstrap.min.js': 'angular-bootstrap/ui-bootstrap.min.js',
                    'js/vendor/angular/ui-bootstrap-tpls.js': 'angular-bootstrap/ui-bootstrap-tpls.js', // with HTML templates
                    'js/vendor/angular/ui-bootstrap-tpls.min.js': 'angular-bootstrap/ui-bootstrap-tpls.min.js',

                    // Material Design for Bootstrap
                    'scss/vendor/bootstrap/material-design/material.scss': 'bootstrap-material-design/dist/css/material.css',
                    //'scss/vendor/bootstrap/material-design/material.scss.map': 'bootstrap-material-design/dist/css/material.css.map',
                    'scss/vendor/bootstrap/material-design/material.min.scss': 'bootstrap-material-design/dist/css/material.min.css',
                    //'scss/vendor/bootstrap/material-design/material.min.scss.map': 'bootstrap-material-design/dist/css/material.min.css.map',
                    'scss/vendor/bootstrap/material-design/material-wfont.scss': 'bootstrap-material-design/dist/css/material-wfont.css',
                    //'scss/vendor/bootstrap/material-design/material-wfont.scss.map': 'bootstrap-material-design/dist/css/material-wfont.css.map',
                    'scss/vendor/bootstrap/material-design/material-wfont.min.scss': 'bootstrap-material-design/dist/css/material-wfont.min.css',
                    //'scss/vendor/bootstrap/material-design/material-wfont.min.scss.map': 'bootstrap-material-design/dist/css/material-wfont.min.css.map',
                    'scss/vendor/bootstrap/material-design/ripples.scss': 'bootstrap-material-design/dist/css/ripples.css',
                    //'scss/vendor/bootstrap/material-design/ripples.scss.map': 'bootstrap-material-design/dist/css/ripples.css.map',
                    'scss/vendor/bootstrap/material-design/ripples.min.scss': 'bootstrap-material-design/dist/css/ripples.min.css',
                    //'scss/vendor/bootstrap/material-design/ripples.min.scss.map': 'bootstrap-material-design/dist/css/ripples.min.css.map',

                    'js/vendor/bootstrap/material-design': 'bootstrap-material-design/dist/js',
                    'fonts/vendor/bootstrap/material-design': 'bootstrap-material-design/dist/fonts'
                }
            },
            almond: {
                options: {
                    destPrefix: '<%= project.src %>/js/vendor'
                },
                files: {
                    'almond.js': 'almond/almond.js'
                }
            },
            use: {
                options: {
                    destPrefix: '<%= project.src %>/js/vendor'
                },
                files: {
                    'use.js': 'use-amd/use.js'
                }
            },
            angular: {
                options: {
                    destPrefix: '<%= project.src %>'
                },
                files: {
                    // AngularJS
                    'js/vendor/angular/angular.js': 'angular/angular.js',
                    'js/vendor/angular/angular.min.js': 'angular/angular.min.js',
                    'js/vendor/angular/angular.min.js.map': 'angular/angular.min.js.map',

                    // angular-ui-router
                    'js/vendor/angular/angular-ui-router.js': 'angular-ui-router/release/angular-ui-router.js',
                    'js/vendor/angular/angular-ui-router.min.js': 'angular-ui-router/release/angular-ui-router.min.js',

                    // angular-route
                    'js/vendor/angular/angular-route.js': 'angular-route/angular-route.js',
                    'js/vendor/angular/angular-route.min.js': 'angular-route/angular-route.min.js',
                    'js/vendor/angular/angular-route.min.js.map': 'angular-route/angular-route.min.js.map',

                    // AngularAMD
                    'js/vendor/angular/angularAMD.js': 'angularAMD/angularAMD.js',
                    'js/vendor/angular/angularAMD.min.js': 'angularAMD/angularAMD.min.js',
                    'js/vendor/angular/angularAMD.min.map': 'angularAMD/angularAMD.min.map',
                    'js/vendor/angular/ngload.js': 'angularAMD/ngload.js',
                    'js/vendor/angular/ngload.min.js': 'angularAMD/ngload.min.js',

                    // ngFX
                    'js/vendor/angular/ngFx.js': 'ngFx/dist/ngFx.js',
                    'js/vendor/angular/ngFx.min.js': 'ngFx/dist/ngFx.min.js',
                    'js/vendor/angular/ngFxBundle.js': 'ngFx/dist/ngFxBundle.js',
                    'js/vendor/angular/ngFxBundle.min.js': 'ngFx/dist/ngFxBundle.min.js',

                    // ngBusy
                    'js/vendor/angular/ngBusy.js': 'angular-busy/dist/angular-busy.js',
                    'js/vendor/angular/ngBusy.min.js': 'angular-busy/dist/angular-busy.min.js',
                    'scss/vendor/angular/ngBusy.scss': 'angular-busy/dist/angular-busy.css',
                    'scss/vendor/angular/ngBusy.min.scss': 'angular-busy/dist/angular-busy.min.css'
                }
            },
            toastr: {
                options: {
                    destPrefix: '<%= project.src %>'
                },
                files: {
                    // css
                    'scss/vendor/angular-toastr.scss': 'angular-toastr/dist/angular-toastr.css',

                    // js
                    'js/vendor/angular/angular-toastr.js': 'angular-toastr/dist/angular-toastr.js',
                    'js/vendor/angular/angular-toastr.tpls.js': 'angular-toastr/dist/angular-toastr.tpls.js' // with HTML templates
                }
            },

            // TODO: maybe not needed
            sugar: {
                options: {
                    destPrefix: '<%= project.src %>'
                },
                files: {
                    //js
                    'js/vendor/sugar.min.js': 'sugar/release/sugar.min.js'
                }
            },

            underscore: {
                options: {
                    destPrefix: '<%= project.src %>'
                },
                files: {
                    //js
                    'js/vendor/underscore.min.js': 'underscore/underscore-min.js',
                    'js/vendor/underscore.min.map': 'underscore/underscore-min.map',
                    'js/vendor/underscore.js': 'underscore/underscore.js'
                }
            }
        },

        /**
         * JSHint
         * https://github.com/gruntjs/grunt-contrib-jshint
         * Manage the options inside .jshintrc file
         */
        jshint: {
            files: ['<%= project.src %>/js/**/*.js', '!<%= project.src %>/js/vendor/**/*.js'],
            options: {
                jshintrc: '.jshintrc',
                force: true
            }
        },

        /**
         * Compile Sass/SCSS files
         * https://github.com/gruntjs/grunt-contrib-sass
         * Compiles all Sass/SCSS files and appends project banner
         */
        sass: {
            dev: {
                options: {
                    style: 'expanded'
                },
                files: {
                    '<%= project.files.css %>': '<%= project.files.scss %>'
                }
            },
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    '<%= project.files.css_min %>': '<%= project.files.scss %>'
                }
            }
        },

        /**
         * Optimize RequireJS projects using r.js
         * https://github.com/gruntjs/grunt-contrib-requirejs
         */
        requirejs: {
            compile: {
                options: {
                    baseUrl: '<%= project.src %>/js',
                    mainConfigFile: '<%= project.src %>/js/config.js',
                    name: 'vendor/almond',
                    optimize: 'uglify', // `uglify` with AngularJS... took 2 days of "debugging" to find the problem... sad story :(
                    uglify: {
                        no_mangle: true // and that's all the solution...
                    },
                    out: '<%= project.assets %>/js/main.min.js',
                    preserveLicenseComments: false,
                    paths: {
                        /*
                         * During the development I prefer to use local versions of libraries and frameworks.
                         * However, in production stage — CDN
                         */

                        'modernizr': 'empty:',
                        'jquery': 'empty:',
                        'angular': 'empty:',
                        'angularUiRouter': 'empty:',
                        'uiBootstrap': 'empty:'
                    }
                }
            }
        },

        /**
         * Runs tasks against changed watched files
         * https://github.com/gruntjs/grunt-contrib-watch
         * Livereload the browser once complete
         */
        watch: {
            sass: {
                files: '<%= project.src %>/scss/{,*/}*.{scss,sass}',
                tasks: ['sass:dev']
            },
            livereload: {
                options: {
                    livereload: 35729
                },
                files: [
                    '<%= project.app %>/{,*/}*.html',
                    '<%= project.assets %>/css/*.css',
                    '<%= project.assets %>/js/{,*/}*.js',
                    '<%= project.assets %>/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        }
    });

    /**
     * Static task
     * Run `grunt` on the command line
     */
    grunt.registerTask('default', [
        'clean',
        'bowercopy',
        'sass',
        'jshint',
        'requirejs',
        'watch'
    ]);

    /**
     * Build task
     * Run `grunt build` on the command line
     */
    grunt.registerTask('build', [
        'clean',
        'bowercopy',
        'sass',
        'jshint',
        'requirejs'
    ]);

    /**
     * Small build task
     * Run `grunt small` on the command line
     */
    grunt.registerTask('small', [
        'clean',
        'bowercopy',
        'jshint',
        'requirejs'
    ]);

    /**
     * Faster build task
     * Run `grunt smaller` on the command line
     */
    grunt.registerTask('smaller', [
        'jshint',
        'requirejs'
    ]);

};
