<?php

namespace PKK\APIBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CourseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',              'text',     array('label' => 'Name'))
            ->add('description',       'text',     array('label' => 'Description'))
            ->add('isCommentable',     'checkbox', array('label' => 'Allow commenting', 'mapped' => false, 'required' => false))
            ->add('start',             'date',     array('label' => 'Course starts', 'required' => false))
            ->add('end',               'date',     array('label' => 'Course ends', 'required' => false))
            ->add('registrationStart', 'date',     array('label' => 'Registration starts', 'required' => false))
            ->add('registrationEnd',   'date',     array('label' => 'Registration ends', 'required' => false))
            ->add('maxParticipants',   'number',   array('label' => 'Maximum participants'))
            ->add('location',          'text',     array('label' => 'Location'))
//            ->add('createdBy',         'entity',   array('label' => 'Created by', 'class' => 'PKK\CoreBundle\Entity\User', 'required' => false))
//            ->add('createdAt')
//            ->add('updatedAt')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PKK\CoreBundle\Entity\Course'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'course';
    }
}
