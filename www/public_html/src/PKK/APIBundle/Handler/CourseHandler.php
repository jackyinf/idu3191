<?php

namespace PKK\APIBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use PKK\APIBundle\Form\Type\CourseType;
use PKK\CoreBundle\Entity\Course;
use PKK\CoreBundle\Exception\InvalidFormException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Course handler.
 *
 * @package PKK\APIBundle\Handler
 */
class CourseHandler implements CourseHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function all($limit = 5, $offset = 0)
    {
        return $this->repository->findBy(array(), null, $limit, $offset);
    }

    /**
     * {@inheritdoc}
     */
    public function post(Request $request)
    {
        $course = $this->createEvent();
        return $this->processForm($course, $request, 'POST');
    }

    private function createEvent()
    {
        return new $this->entityClass();
    }

    /**
     * Processes the form.
     *
     * @param Course  $course
     * @param Request $request
     * @param String  $method
     *
     * @return Course
     * @internal param array $parameters
     */
    private function processForm(Course $course, Request $request, $method = "PUT")
    {
        $form = $this->formFactory->create(new CourseType(), $course, array(
            'method' => $method,
            'csrf_protection' => false
        ));

        $form->handleRequest($request);

        if ($form->isValid()) {
            /* TODO: bad idea to put here, but didn't find a working solution at this point */
            if ($course->getStart() === NULL) $course->setStart(new \DateTime());
            if ($course->getRegistrationStart() === NULL) $course->setRegistrationStart(new \DateTime());

            /** @var \PKK\CoreBundle\Entity\Course $course */
            $this->om->persist($course);
            $this->om->flush($course);

            return $course;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    /**
     * {@inheritdoc}
     */
    public function put(Course $course, Request $request)
    {
        return $this->processForm($course, $request, 'PUT');
    }

    /**
     * {@inheritdoc}
     */
    public function patch(Course $course, Request $request)
    {
        return $this->processForm($course, $request, 'PATCH');
    }
}
