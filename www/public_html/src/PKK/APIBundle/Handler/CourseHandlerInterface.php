<?php
namespace PKK\APIBundle\Handler;

use PKK\CoreBundle\Entity\Course;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface for CourseHandler.
 *
 * @package PKK\APIBundle\Handler
 */
interface CourseHandlerInterface
{
    /**
     * Get an Course given the identifier.
     *
     * @api
     *
     * @param mixed $id
     *
     * @return Course
     */
    public function get($id);

    /**
     * Get a list of Events.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0);

    /**
     * Post Course, creates a new Course.
     *
     * @api
     *
     * @param Request $request
     *
     * @return Course
     *
     * @internal param array $parameters
     */
    public function post(Request $request);

    /**
     * Edit the Course.
     *
     * @api
     *
     * @param Course   $course
     * @param Request $request
     *
     * @return Course
     *
     * @internal param array $parameters
     */
    public function put(Course $course, Request $request);

    /**
     * Partially update an Course.
     *
     * @api
     *
     * @param Course   $course
     * @param Request $request
     *
     * @return Course
     *
     * @internal param array $parameters
     */
    public function patch(Course $course, Request $request);
}
