<?php

namespace PKK\APIBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use PKK\CoreBundle\Entity\FeedbackTemplate;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * API for feedback templates.
 *
 * @package PKK\APIBundle\Controller
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackTemplatesController extends FOSRestController
{
    /**
     * List all feedback templates.
     *
     * @ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         403 = "Returned when access is forbidden"
     *     }
     * )
     *
     * @Rest\View()
     *
     * @return array
     */
    public function allAction()
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            return View::create()
                ->setStatusCode(Codes::HTTP_FORBIDDEN)
                ->setData(array('status' => 'error'));
        }

        $em = $this->getDoctrine()->getManager();

        $templates = $em->getRepository('PKKCoreBundle:FeedbackTemplate')->findAll();

        $view = View::create()
            ->setStatusCode(200)
            ->setSerializationContext(SerializationContext::create()->setGroups(array('api_feedback_template_all')))
            ->setData($templates);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Get a single feedback template.
     *
     * @ApiDoc(
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         403 = "Returned when access is forbidden",
     *         404 = "Returned when the template is not found"
     *     }
     * )
     *
     * @Rest\View(templateVar="template")
     *
     * @param int $id template id
     *
     * @return array
     *
     * @throws NotFoundHttpException when note not exist
     */
    public function getAction($id)
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            return View::create()
                ->setStatusCode(Codes::HTTP_FORBIDDEN)
                ->setData(array('status' => 'error'));
        }

        $em = $this->getDoctrine()->getManager();

        $course = $em->getRepository('PKKCoreBundle:FeedbackTemplate')->find($id);

        if (!$course instanceof FeedbackTemplate) {
            throw new NotFoundHttpException('Feedback template not found');
        }

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setSerializationContext(SerializationContext::create()->setGroups(array('api_feedback_template_get')))
            ->setData($course);

        return $this->get('fos_rest.view_handler')->handle($view);
    }
}
