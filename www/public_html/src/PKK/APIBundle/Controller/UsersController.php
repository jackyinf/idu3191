<?php

namespace PKK\APIBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use PKK\CoreBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * API for users.
 *
 * @package PKK\APIBundle\Controller
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class UsersController extends FOSRestController
{
    /**
     * List all users.
     *
     * @ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         200 = "Returned when successful"
     *     }
     * )
     *
     * @Rest\View()
     *
     * @return array
     */
    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('PKKCoreBundle:User')->findAll();

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setSerializationContext(SerializationContext::create()->setGroups(array('api_user_all')))
            ->setData($users);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Get a single user.
     *
     * @ApiDoc(
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         401 = "Returned when unauthorized access",
     *         403 = "Returned when access is forbidden",
     *         404 = "Returned when the user is not found"
     *     }
     * )
     *
     * @Rest\View(templateVar="user")
     *
     * @param int $id user id
     *
     * @return array
     *
     * @throws NotFoundHttpException when note not exist
     */
    public function getAction($id)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return View::create()->setStatusCode(Codes::HTTP_UNAUTHORIZED);
        } else if (!$this->isGranted('ROLE_ADMIN')) {
            return View::create()->setStatusCode(Codes::HTTP_FORBIDDEN);
        }

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('PKKCoreBundle:User')->find($id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('User not found');
        }

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setSerializationContext(SerializationContext::create()->setGroups(array('api_user_get')))
            ->setData($user);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Get a current logged in user.
     *
     * @ApiDoc(
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         401 = "Returned when unauthorized access",
     *         404 = "Returned when the user is not found"
     *     }
     * )
     *
     * @Rest\View(templateVar="user")
     *
     * @return array
     *
     * @throws NotFoundHttpException when note not exist
     */
    public function getLoggedInAction()
    {
        if (!$this->isGranted('ROLE_USER')) {
            return View::create()->setStatusCode(Codes::HTTP_UNAUTHORIZED);
        }

        $user = $this->getUser();

        if (!$user instanceof User) {
            throw new NotFoundHttpException('User not found');
        }

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setSerializationContext(SerializationContext::create()->setGroups(array('api_user_get')))
            ->setData($user);

        return $this->get('fos_rest.view_handler')->handle($view);
    }
}
