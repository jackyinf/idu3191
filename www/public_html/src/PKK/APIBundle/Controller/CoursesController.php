<?php

namespace PKK\APIBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use PKK\CoreBundle\Entity\Course;
use PKK\CoreBundle\Entity\Participation;
use PKK\CoreBundle\Exception\InvalidFormException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * API for courses.
 *
 * @package PKK\APIBundle\Controller
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class CoursesController extends FOSRestController
{
    /**
     * List all courses.
     *
     * @ApiDoc(
     *     statusCodes = {
     *         200 = "Returned when successful"
     *     }
     * )
     *
     * @Rest\View()
     *
     * @return array
     */
    public function allAction()
    {
        $coursesAll = $this->getAllCourses();

        $courses = new ArrayCollection();

        /** @var \PKK\CoreBundle\Entity\Course $course */
        foreach ($coursesAll as $course) {
            /*
             * OMG, I'm such a lazy piece of shit that doesn't want to make this properly using custom Repository.
             */
            if ($this->checkIfCourseIsAvailable($course)) $courses->add($course);
        }

        if (count($courses) === 0) {
            throw new NotFoundHttpException('Courses not found');
        }

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setSerializationContext(SerializationContext::create()->setGroups(array('api_course_all')))
            ->setData($courses);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * @return mixed
     */
    private function getAllCourses()
    {
        $em = $this->getDoctrine()->getManager();

        return $em->getRepository('PKKCoreBundle:Course')->findAll();
    }

    /**
     * @param Course $course
     *
     * @return bool
     */
    private function checkIfCourseIsAvailable(Course $course)
    {
        /*
         * Please remove this function and make a custom Repository class for Courses and solve this problem using Doctrine.
         */
        $currentDateTime = new \DateTime();

        // deleted
        if ($course->isDeleted()) {
            return false;
        }

        // end date is due
        if ($course->getEnd() !== null) {
            $diff = $course->getEnd()->getTimestamp() - $currentDateTime->getTimestamp();
            if ($diff <= 0) return false;
        }

        // max participants limit reached
        if ($course->getMaxParticipants() !== null &&
            $course->getParticipations()->count() >= $course->getMaxParticipants()
        ) {
            return false;
        }

        // registration not started yet
        if ($course->getRegistrationStart() !== null) {
            $diff = $currentDateTime->getTimestamp() - $course->getRegistrationStart()->getTimestamp();
            if ($diff <= 0) return false;
        }

        // registration already ended
        if ($course->getRegistrationEnd() !== null) {
            $diff = $course->getRegistrationEnd()->getTimestamp() - $currentDateTime->getTimestamp();
            if ($diff <= 0) return false;
        }

        return true;
    }

    /**
     * List all ended courses.
     *
     * @ApiDoc(
     *     statusCodes = {
     *         200 = "Returned when successful"
     *     }
     * )
     *
     * @Rest\View()
     *
     * @return array
     */
    public function allEndedAction()
    {
        $coursesAll = $this->getAllCourses();

        $courses = new ArrayCollection();

        $currentDateTime = new \DateTime();

        /** @var \PKK\CoreBundle\Entity\Course $course */
        foreach ($coursesAll as $course) {
            /*
             * OMG, I'm such a lazy piece of shit that doesn't want to make this properly using custom Repository.
             */
            if (!$this->checkIfCourseIsAvailable($course) && !$course->isDeleted()) {
                $diff = $course->getEnd()->getTimestamp() - $currentDateTime->getTimestamp();
                if ($diff <= 0) $courses->add($course);
            }
        }

        if (count($courses) === 0) {
            throw new NotFoundHttpException('Courses not found');
        }

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setSerializationContext(SerializationContext::create()->setGroups(array('api_course_all')))
            ->setData($courses);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Get a single course.
     *
     * @ApiDoc(
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned when the course not found"
     *     }
     * )
     *
     * @Rest\View(templateVar="course")
     *
     * @param int $id course id
     *
     * @return array
     *
     * @throws NotFoundHttpException when note not exist
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var \PKK\CoreBundle\Entity\Course $course */
        $course = $em->getRepository('PKKCoreBundle:Course')->find($id);

        /*
         * OMG, I'm such a lazy piece of shit that doesn't want to make this properly using custom Repository.
         * Even this comment is duplicated.
         */
        if (!$this->checkIfCourseIsAvailable($course)) $course = null;

        if (!$course instanceof Course) {
            throw new NotFoundHttpException('Course not found');
        } else if ($course instanceof Course && $course->isDeleted()) {
            throw new NotFoundHttpException('Course not found');
        }

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setSerializationContext(SerializationContext::create()->setGroups(array('api_course_get')))
            ->setData($course);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Create a new course.
     *
     * @ApiDoc(
     *     resource = true,
     *     input = "PKK\APIBundle\Form\Type\CourseType",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         400 = "Returned when not valid",
     *         400 = "Returned when no parameters passed",
     *         403 = "Returned when access is forbidden"
     *     }
     * )
     *
     * @Rest\View(templateVar="course")
     *
     * @param Request $request
     *
     * @return array
     */
    public function newAction(Request $request)
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            return View::create()->setStatusCode(Codes::HTTP_FORBIDDEN);
        }

        if (strlen($request->getContent()) === 0) {
            throw new BadRequestHttpException('Bad request');
        }

        try {
            /** @var \PKK\CoreBundle\Entity\Course $course */
            $course = $this->container->get('pkk_api.course.handler')->post($request);

            $routeOptions = array(
                'id'      => $course->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('pkk_api_course_get', $routeOptions, Codes::HTTP_CREATED);
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Remove a course.
     *
     * @ApiDoc(
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         403 = "Returned when access is forbidden",
     *         404 = "Returned when the course not found"
     *     }
     * )
     *
     * @param int $id Course id
     *
     * @return RouteRedirectView
     */
    public function deleteAction($id)
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            return View::create()->setStatusCode(Codes::HTTP_FORBIDDEN);
        }

        $em = $this->getDoctrine()->getManager();

        /** @var \PKK\CoreBundle\Entity\Course $course */
        $course = $em->getRepository('PKKCoreBundle:Course')->find($id);

        if (!$course instanceof Course) {
            throw new NotFoundHttpException('Course not found');
        } else if ($course instanceof Course && $course->isDeleted()) {
            throw new NotFoundHttpException('Course not found');
        }

        $em->remove($course);
        $em->flush();

        $view = View::create()->setStatusCode(Codes::HTTP_NO_CONTENT);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Register to course.
     *
     * @ApiDoc(
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         400 = "Returned when not valid",
     *         401 = "Returned when unauthorized access",
     *         404 = "Returned when the course not found"
     *     }
     * )
     *
     * @Rest\View(templateVar="course")
     *
     * @param int $id Course id
     *
     * @return array
     *
     * @throws NotFoundHttpException when note not exist
     */
    public function registerAction($id)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return View::create()->setStatusCode(Codes::HTTP_UNAUTHORIZED);
        }

        $em = $this->getDoctrine()->getManager();

        /** @var \PKK\CoreBundle\Entity\Course $course */
        $course = $this->getCourse($id);
        if ($course === null) {
            throw new NotFoundHttpException('Course not found');
        }

        /** @var \PKK\CoreBundle\Entity\Participation $participation */
        $participation = $em->getRepository('PKKCoreBundle:Participation')->findOneBy(
            array(
                'user'   => $this->getUser(),
                'course' => $course
            )
        );

        if ($participation instanceof Participation) {
            throw new BadRequestHttpException('Participation already exists');
        }

        $participation = new Participation();
        $participation
            ->setUser($this->getUser())
            ->setCourse($course);

        $em->persist($participation);
        $em->flush();

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setSerializationContext(SerializationContext::create()->setGroups(array('api_course_all')))
            ->setData($participation);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Find existing course by id.
     *
     * @param $id int course id
     *
     * @return Course
     */
    private function getCourse($id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var \PKK\CoreBundle\Entity\Course $course */
        $course = $em->getRepository('PKKCoreBundle:Course')->find($id);

        if (!$course instanceof Course) {
            return null;
        } else if ($course instanceof Course && $course->isDeleted()) {
            return null;
        }

        return $course;
    }

    /**
     * Cancel registration to course.
     *
     * @ApiDoc(
     *     statusCodes = {
     *         204 = "Returned when successful",
     *         401 = "Returned when unauthorized access",
     *         404 = "Returned when the course not found"
     *     }
     * )
     *
     * @Rest\View(templateVar="course")
     *
     * @param int $id Course id
     *
     * @return array
     *
     * @throws NotFoundHttpException when course or participation not found
     */
    public function cancelAction($id)
    {
        if (!$this->isGranted('ROLE_USER')) {
            return View::create()->setStatusCode(Codes::HTTP_UNAUTHORIZED);
        }

        $em = $this->getDoctrine()->getManager();

        /** @var \PKK\CoreBundle\Entity\Course $course */
        $course = $this->getCourse($id);
        if ($course === null) {
            throw new NotFoundHttpException('Course not found');
        }

        /** @var \PKK\CoreBundle\Entity\Participation $participation */
        $participation = $em->getRepository('PKKCoreBundle:Participation')->findOneBy(
            array(
                'user'   => $this->getUser(),
                'course' => $course
            )
        );

        if (!$participation instanceof Participation) {
            throw new NotFoundHttpException('Participation not found');
        }

        $em->remove($participation);
        $em->flush();

        $view = View::create()->setStatusCode(Codes::HTTP_NO_CONTENT);

        return $this->get('fos_rest.view_handler')->handle($view);
    }
}
