<?php

namespace PKK\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StaticController extends Controller
{
    public function indexAction()
    {
        return $this->render('PKKFrontBundle:Static:index.html.twig');
    }

    public function partialAction($name)
    {
        return $this->render('PKKFrontBundle:Static:Partials/' . $name . '.html.twig');
    }

}
