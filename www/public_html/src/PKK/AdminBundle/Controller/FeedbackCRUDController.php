<?php

namespace PKK\AdminBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Admin CRUD for feedback.
 *
 * @package PKK\AdminBundle\Controller
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackCRUDController extends Controller
{
    public function restoreAction()
    {
        $feedback = $this->admin->getSubject();

        if (!$feedback) {
            throw new NotFoundHttpException(sprintf('Unable to find the object with id: %s', $id));
        }

        $feedback->restore();

        $this->admin->update($feedback);

        $this->addFlash('sonata_flash_success', 'Restored successfully');

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}
