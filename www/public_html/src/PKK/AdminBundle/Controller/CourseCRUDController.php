<?php

namespace PKK\AdminBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Admin CRUD for courses.
 *
 * @package PKK\AdminBundle\Controller
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class CourseCRUDController extends Controller
{
    public function restoreAction()
    {
        /** @var \PKK\CoreBundle\Entity\Course $course */
        $course = $this->admin->getSubject();

        if (!$course) {
            throw new NotFoundHttpException(sprintf('Unable to find the object with id: %s', $id));
        }

        $course->restore();

        $this->admin->update($course);

        $this->addFlash('sonata_flash_success', 'Restored successfully');

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function cloneAction()
    {
        /** @var \PKK\CoreBundle\Entity\Course $object */
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('Unable to find the object with id: %s', $id));
        }

        $clonedObject = clone $object;  // Careful, you may need to overload the __clone method of your object
        // to set its id to null
        $clonedObject->setName($object->getName() . " (Clone)");

        $this->admin->create($clonedObject);

        $this->addFlash('sonata_flash_success', 'Cloned successfully');

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}
