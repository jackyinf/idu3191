<?php

namespace PKK\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Admin class for participation.
 *
 * @package PKK\AdminBundle\Admin
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class ParticipationAdmin extends PKKAdmin
{
    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        /** @var \PKK\CoreBundle\Entity\Participation $participation */
        $participation = parent::getNewInstance();

//        $user = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();

        $participation->setConfirmed(true);

        return $participation;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array(
                'class'     => 'col-md-6',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('user')
//            ->add('course')
            ->add('confirmed', null, array('required' => false))
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('user')
            ->add('course');
    }
}
