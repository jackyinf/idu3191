<?php

namespace PKK\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Mapper\BaseGroupedMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Admin class for courses.
 *
 * @package PKK\AdminBundle\Admin
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class CourseAdmin extends PKKAdmin
{
    /**
     * {@inheritdoc}
     */
    public function preUpdate($object)
    {
        /** @var \PKK\CoreBundle\Entity\Course $object */
        /** @var \PKK\CoreBundle\Entity\Participation $participation */
        foreach ($object->getParticipations() as $participation) {
            if ($participation->getCourse() === null) {
                $participation->setCourse($object);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->generalBlock($formMapper);
    }

    /**
     * @param BaseGroupedMapper $mapper
     *
     * @return BaseGroupedMapper|FormMapper|ShowMapper
     */
    private function generalBlock(BaseGroupedMapper $mapper)
    {
        return $mapper
            ->with('Participations', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-6',
                'box_class' => 'box box-solid box-default',
            ))
            ->add('participations', 'sonata_type_collection', array(), array(
                'edit'   => 'inline',
                'inline' => 'table'
            ))
            ->end()
            ->with('General', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-6',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('name', 'text', array('label' => 'Name'))
            ->add('description', null, array('label' => 'Description'))
            ->add('location', 'text', array('label' => 'Location'))
            ->add('maxParticipants', 'number', array('label' => 'Maximum participants'))
            ->add('createdBy', 'entity', array('label' => 'Created by', 'class' => 'PKK\CoreBundle\Entity\User'))
            ->end()
            ->with('Starts/ends', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-3',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('start', 'date', array('label' => 'Course starts'))
            ->add('end', 'date', array('label' => 'Course ends'))
            ->add('registrationStart', 'date', array('label' => 'Registration starts'))
            ->add('registrationEnd', 'date', array('label' => 'Registration ends'))
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('location')
            ->add('maxParticipants')
            ->add('createdBy');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $this->generalBlock($showMapper);
        $this->blockAdditional($showMapper);
        $this->blockDeleted($showMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $this->fieldId($listMapper);

        $listMapper
            ->addIdentifier('name')
            ->add('location')
            ->add('maxParticipants', null, array('label' => 'Max participants'))
            ->add('createdBy', null, array('label' => 'Created by'));

        $this->fieldDeleted($listMapper);
        $this->fieldAction($listMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('restore', $this->getRouterIdParameter() . '/restore');
    }
}
