<?php

namespace PKK\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Mapper\BaseGroupedMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Abstract admin class for administration.
 *
 * @package PKK\AdminBundle\Admin
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
abstract class PKKAdmin extends Admin
{
    protected function blockAdditional(BaseGroupedMapper $mapper)
    {
        return $mapper
            ->with('Additional', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-3',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('createdAt', null, array('label' => 'Created at'))
            ->add('updatedAt', null, array('label' => 'Updated at'))
            ->end();
    }

    protected function blockDeleted(BaseGroupedMapper $mapper)
    {
        $subject = $mapper->getAdmin()->getSubject();

        if ($subject->isDeleted()) {
            $mapper
                ->with('Deleted', array(
                    'class'     => 'col-sm-12 col-md-6 col-lg-3',
                    'box_class' => 'box box-solid box-danger'
                ))
                ->add('deletedBy')
                ->add('deletedAt')
                ->end();
        }

        return $mapper;
    }

    protected function fieldAction(ListMapper $mapper)
    {
        return $mapper
            ->add('_action', 'actions', array(
                'label'   => 'Actions',
                'actions' => array(
                    'show'    => array(),
                    'edit'    => array(),
                    'delete'  => array(
                        'template' => 'PKKAdminBundle:CRUD:list__action_delete.html.twig'),
                    'restore' => array(
                        'template' => 'PKKAdminBundle:CRUD:list__action_restore.html.twig'
                    ),
                )
            ));
    }

    protected function fieldId(ListMapper $mapper)
    {
        return $mapper
            ->addIdentifier('id', null, array(
                'label'    => 'ID',
                'template' => 'PKKAdminBundle:CRUD:list_id.html.twig'
            ));
    }

    protected function fieldDeleted(ListMapper $mapper)
    {
        return $mapper
            ->add('deleted', 'boolean', array(
                'label'    => 'Availability',
                'template' => 'PKKAdminBundle:CRUD:list_deleted.html.twig'
            ));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('restore', $this->getRouterIdParameter() . '/restore');
    }
}
