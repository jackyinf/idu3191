<?php

namespace PKK\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Admin class for feedback templates.
 *
 * @package PKK\AdminBundle\Admin
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackTemplateAdmin extends PKKAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Questions', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-6',
                'box_class' => 'box box-solid box-default',
            ))
            ->add('questions', array(
                'name' => ''
            ))
            ->end()
            ->with('Courses', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-6',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('courses')
            ->end()
            ->with('General', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-3',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('name')
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('courses');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Questions', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-6',
                'box_class' => 'box box-solid box-default',
            ))
            ->add('questions')
            ->end()
            ->with('Courses', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-6',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('courses')
            ->end()
            ->with('General', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-3',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('name')
            ->end();

        $this->blockAdditional($showMapper);
        $this->blockDeleted($showMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $this->fieldId($listMapper)->addIdentifier('name');
        $this->fieldDeleted($listMapper);
        $this->fieldAction($listMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('restore', $this->getRouterIdParameter() . '/restore');
    }
}
