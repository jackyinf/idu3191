<?php

namespace PKK\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Admin class for feedback answers.
 *
 * @package PKK\AdminBundle\Admin
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackAnswerAdmin extends PKKAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('createdBy');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-3',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('question')
            ->add('content')
            ->add('createdBy', null, array('label' => 'Created by'))
            ->end();

        $this->blockAdditional($showMapper);
        $this->blockDeleted($showMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $this->fieldId($listMapper)
            ->add('question')
            ->add('content')
            ->add('createdBy', null, array('label' => 'Created by'));

        $this->fieldDeleted($listMapper);
        $this->fieldAction($listMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('restore', $this->getRouterIdParameter() . '/restore');

        $collection->remove('edit');
        $collection->remove('create');
    }
}
