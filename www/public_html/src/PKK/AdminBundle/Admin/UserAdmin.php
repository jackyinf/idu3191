<?php

namespace PKK\AdminBundle\Admin;

use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Admin class for users.
 *
 * @package PKK\AdminBundle\Admin
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class UserAdmin extends PKKAdmin
{
    /**
     * {@inheritdoc}
     */
    public function getNewInstance()
    {
        /** @var \PKK\CoreBundle\Entity\User $user */
        $user = parent::getNewInstance();

        $user->setEnabled(true);

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormBuilder()
    {
        $this->formOptions['data_class'] = $this->getClass();

        $options = $this->formOptions;
        $options['validation_groups'] = (!$this->getSubject()
            || is_null(
                $this->getSubject()->getId()
            )) ? 'Registration' : 'Profile';

        $formBuilder = $this->getFormContractor()->getFormBuilder(
            $this->getUniqid(),
            $options
        );

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        // avoid security field to be exported
        return array_filter(
            parent::getExportFields(),
            function ($v) {
                return !in_array($v, array('password', 'salt'));
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($user)
    {
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }

    /**
     * @param UserManagerInterface $userManager
     */
    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add('id', null, array(
                'label'    => 'ID',
                'template' => 'PKKAdminBundle:CRUD:list_id.html.twig'
            ))
            ->addIdentifier('username')
            ->add('name')
            ->add('email')
            ->add('groups')
            ->add('enabled', null, array('editable' => true))
            ->add('locked', null, array('editable' => true))
            ->add('_action', 'actions', array(
                'label'   => 'Actions',
                'actions' => array(
                    'show'   => array(),
                    'edit'   => array(),
                    'delete' => array(),
                )
            ));

//        if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
//            $listMapper
//                ->add(
//                    'impersonating',
//                    'string',
//                    array('template' => 'SonataUserBundle:Admin:Field/impersonating.html.twig')
//                );
//        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('id')
            ->add('username')
            ->add('locked')
            ->add('email')
            ->add('groups');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General', array(
                'class'     => 'col-md-4',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('username')
            ->add('email')
            ->add('enabled', null, array('label' => 'Is enabled?'))
            ->add('groups')
            ->end()
            ->with('Profile', array(
                'class'     => 'col-md-4',
                'box_class' => 'box box-solid box-default'
            ))
            ->add(
                'gender',
                'sonata_user_gender',
                array(
                    'required'           => true,
                    'translation_domain' => $this->getTranslationDomain()
                )
            )
            ->add('timezone')
            ->add('phone')
            ->add('organization', null, array('label' => 'Organization'))
            ->end()
            ->with('Additional', array(
                'class'     => 'col-md-4',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('createdAt', null, array('label' => 'Created at'))
            ->add('updatedAt', null, array('label' => 'Updated at'))
            ->add('lastLogin', null, array('label' => 'Last login'))
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array(
                'class'     => 'col-md-4',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('username')
            ->add('email')
            ->add(
                'plainPassword',
                'text',
                array(
                    'required' => (!$this->getSubject()
                        || is_null(
                            $this->getSubject()->getId()
                        ))
                )
            );

        if ($this->getSubject()
            && !$this->getSubject()->hasRole(
                'ROLE_SUPER_ADMIN'
            )
        ) {
            $formMapper
                //                ->add(
                //                    'realRoles',
                //                    'sonata_security_roles',
                //                    array(
                //                        'label' => 'form.label_roles',
                //                        'expanded' => true,
                //                        'multiple' => true,
                //                        'required' => false
                //                    )
                //                )
                ->add('locked', null, array('horizontal_input_wrapper_class' => 'new', 'required' => false))
                ->add('expired', null, array('required' => false))
                ->add('enabled', null, array('required' => false))
                ->add('credentialsExpired', null, array('required' => false))
                ->end();
        }

        $formMapper->end()
            ->with('Profile', array(
                'class'     => 'col-md-4',
                'box_class' => 'box box-solid box-default'
            ))
            ->add(
                'gender',
                'sonata_user_gender',
                array(
                    'required'           => true,
                    'translation_domain' => $this->getTranslationDomain()
                )
            )
            ->add('timezone', 'timezone', array('required' => false))
            ->add('phone', null, array('required' => false))
            ->add('organization', null, array('label' => 'Organization', 'required' => false))
            ->end()
            ->with('Groups', array(
                'class'     => 'col-md-4',
                'box_class' => 'box box-solid box-default'
            ))
            ->add(
                'groups',
                'sonata_type_model',
                array(
                    'required' => false,
                    'expanded' => true,
                    'multiple' => true
                )
            )
            ->end();
    }
}
