<?php

namespace PKK\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Admin class for feedback questions.
 *
 * @package PKK\AdminBundle\Admin
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackQuestionAdmin extends PKKAdmin
{
    /**
     * {@inheritdoc}
     */
    public function preUpdate($object)
    {
        /** @var \PKK\CoreBundle\Entity\FeedbackQuestion $object */
        /** @var \PKK\CoreBundle\Entity\FeedbackQuestionTranslation $translation */
        foreach ($object->getTranslations() as $translation) {
            if ($translation->getTranslatable() === null) {
                $translation->setTranslatable($object);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Translations', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-6',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('translations', 'sonata_type_collection', array(), array(
                'edit'   => 'inline',
                'inline' => 'table'
            ))
            ->end()
            ->with('Templates', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-6',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('templates')
            ->end()
            ->with('General', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-3',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('type')
            ->add('group')
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('type')
            ->add('group');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        /** @var \PKK\CoreBundle\Entity\FeedbackQuestion $subject */
        $subject = $showMapper->getAdmin()->getSubject();

        if ($subject->getTranslations()->count() > 0) {
            $showMapper
                ->with('Translations', array(
                    'class'     => 'col-sm-12 col-md-6 col-lg-6',
                    'box_class' => 'box box-solid box-default'
                ))
                ->add('translations')
                ->end();
        }

        if ($subject->getTemplates()->count() > 0) {
            $showMapper
                ->with('Templates', array(
                    'class'     => 'col-sm-12 col-md-6 col-lg-6',
                    'box_class' => 'box box-solid box-default'
                ))
                ->add('templates')
                ->end();
        }

        if ($subject->getAnswers()->count() > 0) {
            $showMapper
                ->with('Answers', array(
                    'class'     => 'col-sm-12 col-md-6 col-lg-6',
                    'box_class' => 'box box-solid box-default'
                ))
                ->add('answers')
                ->end();
        }

        $showMapper
            ->with('General', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-3',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('type')
            ->add('group')
            ->end();

        $this->blockAdditional($showMapper);
        $this->blockDeleted($showMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $this->fieldId($listMapper)
            ->add('content', null, array('label' => 'Question'))
            ->add('type', 'text', array(
                'template' => 'PKKAdminBundle:CRUD:list_feedback_question_type.html.twig'
            ))
            ->add('group');

        $this->fieldDeleted($listMapper);
        $this->fieldAction($listMapper);
    }
}
