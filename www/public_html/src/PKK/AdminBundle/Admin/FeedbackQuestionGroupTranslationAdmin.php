<?php

namespace PKK\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;

/**
 * Admin class for feedback question group translations.
 *
 * @package PKK\AdminBundle\Admin
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackQuestionGroupTranslationAdmin extends PKKAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array(
                'class'     => 'col-md-6',
                'box_class' => 'box box-solid box-default'
            ))
//            ->add('translatable')
            ->add('locale')
            ->add('name', 'text')
            ->end();
    }
}
