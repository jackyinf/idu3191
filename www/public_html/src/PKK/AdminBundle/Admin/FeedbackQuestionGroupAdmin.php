<?php

namespace PKK\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Admin class for feedback question groups.
 *
 * @package PKK\AdminBundle\Admin
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackQuestionGroupAdmin extends PKKAdmin
{
    /**
     * {@inheritdoc}
     */
    public function preUpdate($object)
    {
        /** @var \PKK\CoreBundle\Entity\FeedbackQuestion $object */
        /** @var \PKK\CoreBundle\Entity\FeedbackQuestionTranslation $translation */
        foreach ($object->getTranslations() as $translation) {
            if ($translation->getTranslatable() === null) {
                $translation->setTranslatable($object);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Translations', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-6',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('translations', 'sonata_type_collection', array(), array(
                'edit'   => 'inline',
                'inline' => 'table'
            ))
            ->end()
            ->with('Questions', array(
                'class'     => 'col-sm-12 col-md-6 col-lg-6',
                'box_class' => 'box box-solid box-default'
            ))
            ->add('questions')
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        /** @var \PKK\CoreBundle\Entity\FeedbackQuestionGroup $subject */
        $subject = $showMapper->getAdmin()->getSubject();

        if ($subject->getTranslations()->count() > 0) {
            $showMapper
                ->with('Translations', array(
                    'class'     => 'col-sm-12 col-md-6 col-lg-6',
                    'box_class' => 'box box-solid box-default'
                ))
                ->add('translations')
                ->end();
        }

        $this->blockAdditional($showMapper);
        $this->blockDeleted($showMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $this->fieldId($listMapper)->add('name');
        $this->fieldDeleted($listMapper);
        $this->fieldAction($listMapper);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('restore', $this->getRouterIdParameter() . '/restore');
    }
}
