<?php

namespace PKK\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use PKK\CoreBundle\Entity\User;
use PKK\FixturesBundle\DataFixtures\DataFixture;

/**
 * Users.
 *
 * @package PKK\CoreBundle\DataFixtures\ORM
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class LoadUsers extends DataFixture
{
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('Jack Sparrow');
        $user->setEmail('jack.sparrow@example.com');
        $user->setUsername('Jack');
        $user->setPlainPassword('123456');
        $user->setGender(User::GENDER_MALE);
        $user->setPhone('55666666');
        $user->setOrganization('Black Pearl OÜ');
        $user->setEnabled(true);
        $user->addGroup($this->getReference('pkk.group.administrator'));

        $this->setReference('pkk.user.jack', $user);
        $manager->persist($user);

        $user = new User();
        $user->setName('President Snow');
        $user->setEmail('Coriolanus.Snow@example.com');
        $user->setUsername('Snow');
        $user->setPlainPassword('123456');
        $user->setGender(User::GENDER_MALE);
        $user->setPhone('55666333');
        $user->setOrganization('The Capitol OÜ');
        $user->setEnabled(true);
        $user->addGroup($this->getReference('pkk.group.administrator'));

        $this->setReference('pkk.user.snow', $user);
        $manager->persist($user);

        $user = new User();
        $user->setName('Katniss Everdeen');
        $user->setEmail('Katniss.Everdeen@example.com');
        $user->setUsername('Katniss');
        $user->setPlainPassword('123456');
        $user->setGender(User::GENDER_FEMALE);
        $user->setOrganization('District 12 OÜ');
        $user->setEnabled(true);
        $user->addGroup($this->getReference('pkk.group.participant'));

        $this->setReference('pkk.user.katniss', $user);
        $manager->persist($user);

        $user = new User();
        $user->setName('Peeta Mellark');
        $user->setEmail('Peeta.Mellark@example.com');
        $user->setUsername('Peeta');
        $user->setPlainPassword('123456');
        $user->setGender(User::GENDER_MALE);
        $user->setOrganization('District 12 OÜ');
        $user->setEnabled(true);
        $user->addGroup($this->getReference('pkk.group.participant'));

        $this->setReference('pkk.user.peeta', $user);
        $manager->persist($user);

        $manager->flush(); // Save changes in database.
    }
}
