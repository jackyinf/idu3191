<?php

namespace PKK\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use PKK\CoreBundle\Entity\FeedbackQuestion;
use PKK\CoreBundle\Entity\FeedbackQuestionGroup;
use PKK\CoreBundle\Entity\FeedbackQuestionType;
use PKK\CoreBundle\Entity\FeedbackTemplate;
use PKK\FixturesBundle\DataFixtures\DataFixture;

/**
 * Courses.
 *
 * @package PKK\CoreBundle\DataFixtures\ORM
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class LoadFeedbacks extends DataFixture
{
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 6;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $feedback = array(
            'Данные о курсах'             => array(
                'questions' => array(
                    'Наименование курсов',
                    'Лекторы',
                )
            ),
            'Общая оценка курсам'         => array(
                'questions' => array(
                    'Как Вы удовлетворены проведенными курсами?'                                  => array(
                        'type' => 'scale'
                    ),
                    'Как помогло участие в курсах совершенствовать Ваши профессиональные умения?' => array(
                        'type' => 'scale'
                    ),
                    'Как Вы удовлетворены атмосферой в группе во время курсов?'                   => array(
                        'type' => 'scale'
                    )
                )
            ),
            'Содержание курсов'           => array(
                'questions' => array(
                    'Как Вы удовлетворены  знаниями, полученными в ходе работы в аудитории?' => array(
                        'type' => 'scale'
                    ),
                    'Как Вы оцениваете профессиональность прохождения тем курсов лектором?'  => array(
                        'type' => 'scale'
                    ),
                    'Как Вы оцениваете понятливость представления тем лектором?'             => array(
                        'type' => 'scale'
                    ),
                    'Как Вы оцениваете умение лектора  общаться с аудиторией?'               => array(
                        'type' => 'scale'
                    )
                )
            ),
            'Общетехническая организация' => array(
                'questions' => array(
                    'Как оцениваете материал (слайды, копии и др.) курсов?' => array(
                        'type' => 'scale'
                    ),
                    'Как оцениваете технические средства курсов?'           => array(
                        'type' => 'scale'
                    ),
                    'Как оцениваете помещения обучения?'                    => array(
                        'type' => 'scale'
                    ),
                    'Ваши предложения и комментарии по курсам в том числе в какой области пожелали бы в будущем дополнительное обучение'
                )
            ),
            'Фоновые данные'              => array(
                'questions' => array(
                    'Дата заполнения листка обратной связи',
                    'Пол (М/Ж)'
                )
            )
        );

        $this->loadQuestionTypes($manager, array('text', 'scale'));
        $this->loadTemplate($manager, $feedback);
    }

    private function loadQuestionTypes(ObjectManager $manager, $types)
    {
        if (count($types) == 0) return;

        foreach ($types as $type) {
            $questionType = new FeedbackQuestionType();
            $questionType->setType($type);
            $this->setReference('pkk.feedback.question.type.' . $type, $questionType);

            $manager->persist($questionType);
        }

        $manager->flush(); // Save changes in database.
    }

    private function loadTemplate(ObjectManager $manager, $feedback, $templateName = 'Default')
    {
        if (count($feedback) == 0) return;

        $allQuestions = new ArrayCollection();

        foreach ($feedback as $group => $value) {
            $questions = $value['questions'];
            $lang = isset($value['lang']) ? $value['lang'] : 'ru';
            $user = isset($value['user']) ? $value['user'] : 'snow';

            $groupObject = new FeedbackQuestionGroup();

            /** @var \PKK\CoreBundle\Entity\FeedbackQuestionGroupTranslation $groupObjectTranslation */
            $groupObjectTranslation = $groupObject->translate($lang);
            $groupObjectTranslation->setName($group);
            $groupObject->mergeNewTranslations();
            $groupObject->setCreatedBy($this->getReference('pkk.user.' . $user));

            $manager->persist($groupObject);

            $manager->flush(); // Save changes in database.

            foreach ($questions as $question => $options) {
                $type = isset($options['type']) ? $options['type'] : 'text';
                $lang = isset($options['lang']) ? $options['lang'] : 'ru';
                $user = isset($options['user']) ? $options['user'] : 'snow';

                if (is_int($question)) $question = $options;

                $questionObject = new FeedbackQuestion();
                $questionObject
                    ->setCreatedBy($this->getReference('pkk.user.' . $user))
                    ->setType($this->getReference('pkk.feedback.question.type.' . $type))
                    ->setGroup($groupObject);

                /** @var \PKK\CoreBundle\Entity\FeedbackQuestionTranslation $questionObjectTranslation */
                $questionObjectTranslation = $questionObject->translate($lang);
                $questionObjectTranslation
                    ->setQuestion($questionObject)
                    ->setContent($question);
                $questionObject->mergeNewTranslations();

                $allQuestions->add($questionObject);

                $manager->persist($questionObject);
            }
        }

        $templateObject = new FeedbackTemplate();
        $templateObject
            ->setName($templateName)
            ->setQuestions($allQuestions)
            ->setCourses(new ArrayCollection(array($this->getReference('pkk.course.hunger-games'))));

        $manager->persist($templateObject);

        $manager->flush(); // Save changes in database.
    }
}
