<?php

namespace PKK\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use PKK\CoreBundle\Entity\Participation;
use PKK\FixturesBundle\DataFixtures\DataFixture;

/**
 * Participation.
 *
 * @package PKK\CoreBundle\DataFixtures\ORM
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class LoadParticipation extends DataFixture
{
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $participation = new Participation();
        $participation->setCourse($this->getReference('pkk.course.hunger-games'));
        $participation->setUser($this->getReference('pkk.user.katniss'));
        $participation->setConfirmed(true);

        $this->setReference('pkk.participation.katniss', $participation);
        $manager->persist($participation);

        $participation = new Participation();
        $participation->setCourse($this->getReference('pkk.course.hunger-games'));
        $participation->setUser($this->getReference('pkk.user.peeta'));
        $participation->setConfirmed(true);

        $this->setReference('pkk.participation.peeta', $participation);
        $manager->persist($participation);

        $manager->flush(); // Save changes in database.
    }
}
