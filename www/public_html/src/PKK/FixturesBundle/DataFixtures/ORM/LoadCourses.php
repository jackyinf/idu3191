<?php

namespace PKK\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use PKK\CoreBundle\Entity\Course;
use PKK\FixturesBundle\DataFixtures\DataFixture;

/**
 * Courses.
 *
 * @package PKK\CoreBundle\DataFixtures\ORM
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class LoadCourses extends DataFixture
{
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $event = new Course();
        $event->setName('Hunger Games');
        $event->setDescription('A morbid and brutal competition which takes place annually in the country of Panem. Every year, one boy and one girl between the ages of 12 and 18 were selected from each of the twelve districts as tributes, who train for a week to be gladiators and then are sent into an outdoor arena to fight to the death.');
        $event->setMaxParticipants(24);
        $event->setLocation('Clock Arena');
        $event->setCreatedBy($this->getReference('pkk.user.snow'));

        $this->setReference('pkk.course.hunger-games', $event);
        $manager->persist($event);

        $manager->flush(); // Save changes in database.
    }
}
