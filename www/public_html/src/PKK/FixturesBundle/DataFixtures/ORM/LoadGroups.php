<?php

namespace PKK\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use PKK\CoreBundle\Entity\Group;
use PKK\FixturesBundle\DataFixtures\DataFixture;

/**
 * User groups.
 *
 * @package PKK\CoreBundle\DataFixtures\ORM
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class LoadGroups extends DataFixture
{
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $group = new Group("Administrator", array(), 'Administrator has access to administration panel and can manage everything');
        $group->addRole('ROLE_SUPER_ADMIN');
        $group->addRole('ROLE_SONATA_ADMIN');
        $this->setReference('pkk.group.administrator', $group);

        $manager->persist($group);

        $group = new Group("Participant", array(), 'Participant can register to events');
        $group->addRole('ROLE_PARTICIPANT');
        $this->setReference('pkk.group.participant', $group);

        $manager->persist($group);

        $manager->flush(); // Save changes in database.
    }
}
