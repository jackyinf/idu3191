<?php

namespace PKK\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use PKK\CoreBundle\Entity\Comment;
use PKK\FixturesBundle\DataFixtures\DataFixture;

/**
 * Comments.
 *
 * @package PKK\CoreBundle\DataFixtures\ORM
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class LoadComments extends DataFixture
{
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // Katniss
        $comment = new Comment();
        $comment->setCourse($this->getReference('pkk.course.hunger-games'));
        $comment->setAuthor($this->getReference('pkk.user.katniss'));
        $comment->setBody('"I volunteer as tribute!" — why did I say that? So dumb! How can I cancel my participation in this event?');

        $this->setReference('pkk.comment.katniss', $comment);
        $manager->persist($comment);

        $manager->flush(); // Save changes in database.

        // Snow
        $anwser = new Comment();
        $anwser->setParent($comment);
        $anwser->setCourse($this->getReference('pkk.course.hunger-games'));
        $anwser->setAuthor($this->getReference('pkk.user.snow'));
        $anwser->setBody('You can\'t. But don\'t worry, I will send you some roses to cheer you up!');

        $this->setReference('pkk.comment.snow', $anwser);
        $manager->persist($anwser);

        // Peeta
        $comment = new Comment();
        $comment->setCourse($this->getReference('pkk.course.hunger-games'));
        $comment->setAuthor($this->getReference('pkk.user.peeta'));
        $comment->setBody('Why me? I don\'t want to go either! I can\'t find that "Cancel" button…');

        $this->setReference('pkk.comment.peeta', $comment);
        $manager->persist($comment);

        // Jack
        $comment = new Comment();
        $comment->setCourse($this->getReference('pkk.course.hunger-games'));
        $comment->setAuthor($this->getReference('pkk.user.jack'));
        $comment->setBody('Am I in the wrong story? Well, I\'m Captain Jack Sparrow, so it\'s not a surprise that I rule even in this ocean. Savvy?');

        $this->setReference('pkk.comment.jack', $comment);
        $manager->persist($comment);

        $manager->flush(); // Save changes in database.
    }
}
