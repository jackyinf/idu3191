<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Knp\DoctrineBehaviors\Model\Translatable\Translatable;
use Knp\DoctrineBehaviors\Model\Translatable\Translation;

/**
 * Interface for FeedbackQuestionGroup Entity.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
interface FeedbackQuestionGroupInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id);

    /**
     * @return Collection|FeedbackQuestion[]
     */
    public function getQuestions();

    /**
     * @param Collection|FeedbackQuestion[] $questions
     *
     * @return self
     */
    public function setQuestions($questions);

    /**
     * @param Collection $translations
     *
     * @return self
     */
    public function setTranslations(Collection $translations);

    /**
     * @param Translation $translation
     *
     * @return Translatable
     */
    public function addTranslation($translation);

    /**
     * @param Translation $translation
     *
     * @return Translatable
     */
    public function removeTranslation($translation);
}
