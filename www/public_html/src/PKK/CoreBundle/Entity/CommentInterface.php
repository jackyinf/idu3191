<?php

namespace PKK\CoreBundle\Entity;

use FOS\CommentBundle\Model\SignedCommentInterface;

/**
 * Interface for Comment Entity.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
interface CommentInterface extends SignedCommentInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAuthor();

    /**
     * {@inheritdoc}
     *
     * @param \Symfony\Component\Security\Core\User\UserInterface $author
     *
     * @return self
     */
    public function setAuthor(\Symfony\Component\Security\Core\User\UserInterface $author);

    /**
     * @return CourseInterface
     */
    public function getCourse();

    /**
     * @param CourseInterface $course
     *
     * @return self
     */
    public function setCourse(CourseInterface $course);
}
