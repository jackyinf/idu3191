<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Question in feedback.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackQuestion implements FeedbackQuestionInterface
{
    use ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable,
        ORMBehaviors\Timestampable\Timestampable;

    use ORMBehaviors\Translatable\Translatable {
        addTranslation as addT;
        removeTranslation as removeT;
    }

    /**
     * Unique identifier.
     *
     * @var int
     */
    protected $id;

    /**
     * Question type.
     *
     * @var FeedbackQuestionType
     */
    protected $type;

    /**
     * Question group.
     *
     * @var FeedbackQuestionGroup
     */
    protected $group;

    /**
     * Question templates.
     *
     * @var Collection|FeedbackTemplateInterface[]
     */
    protected $templates;

    /**
     * Answers to current question.
     *
     * @var Collection|FeedbackAnswerInterface[]
     */
    protected $answers;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function setType(FeedbackQuestionType $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * {@inheritdoc}
     */
    public function setGroup(FeedbackQuestionGroup $group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->getContent() !== '') ? $this->getContent() : 'Question';
    }

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        if ($this->getTranslations() !== null && $this->getTranslations()->count() > 0) {
            /** @var \PKK\CoreBundle\Entity\FeedbackQuestionTranslation $translation */
            $translation = $this->translate($this->getCurrentLocale());
            $content = $translation->getContent();

            return (!empty($content)) ? $content : $this->getTranslations()->first()->getContent();
        }

        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function setTranslations(Collection $translations)
    {
        $this->translations = $translations;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addTranslation($translation)
    {
        $translation->setTranslatable($this);

        if ($translation->getLocale() === null) $translation->setLocale($this->getCurrentLocale());

        $this->getTranslations()->set($translation->getLocale(), $translation);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeTranslation($translation)
    {
        $translation->setTranslatable($this);

        $this->getTranslations()->removeElement($translation);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * {@inheritdoc}
     */
    public function setTemplates(Collection $templates)
    {
        $this->templates = $templates;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * {@inheritdoc}
     */
    public function setAnswers(Collection $answers)
    {
        $this->answers = $answers;
    }
}
