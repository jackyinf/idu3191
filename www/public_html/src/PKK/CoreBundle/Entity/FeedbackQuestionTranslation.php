<?php

namespace PKK\CoreBundle\Entity;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Translation for question in feedback.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackQuestionTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * Question content.
     *
     * @var string
     */
    protected $content;

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->getContent() !== null) ? $this->getContent() . ' (' . $this->getLocale() . ')' : 'Translation';
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return FeedbackQuestion
     */
    public function getQuestion()
    {
        return $this->getTranslatable();
    }

    /**
     * @param FeedbackQuestion $question
     *
     * @return self
     */
    public function setQuestion(FeedbackQuestion $question)
    {
        $this->setTranslatable($question);

        return $this;
    }
}
