<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Knp\DoctrineBehaviors\Model\Translatable\Translatable;
use Knp\DoctrineBehaviors\Model\Translatable\Translation;

/**
 * Interface for FeedbackQuestion Entity.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
interface FeedbackQuestionInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id);

    /**
     * @return FeedbackQuestionType
     */
    public function getType();

    /**
     * @param FeedbackQuestionType $type
     *
     * @return self
     */
    public function setType(FeedbackQuestionType $type);

    /**
     * @return FeedbackQuestionGroup
     */
    public function getGroup();

    /**
     * @param FeedbackQuestionGroup $group
     *
     * @return self
     */
    public function setGroup(FeedbackQuestionGroup $group);

    /**
     * @return Collection
     */
    public function getTranslations();

    /**
     * @param Collection $translations
     *
     * @return self
     */
    public function setTranslations(Collection $translations);

    /**
     * @param Translation $translation
     *
     * @return Translatable
     */
    public function addTranslation($translation);

    /**
     * @param Translation $translation
     *
     * @return Translatable
     */
    public function removeTranslation($translation);

    /**
     * @return Collection|FeedbackTemplate[]
     */
    public function getTemplates();

    /**
     * @param Collection|FeedbackTemplate[] $templates
     *
     * @return self
     */
    public function setTemplates(Collection $templates);

    /**
     * @return Collection|FeedbackAnswerInterface[]
     */
    public function getAnswers();

    /**
     * @param Collection|FeedbackAnswerInterface[] $answers
     *
     * @return self
     */
    public function setAnswers(Collection $answers);
}
