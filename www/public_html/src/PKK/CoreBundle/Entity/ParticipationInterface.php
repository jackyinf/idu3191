<?php

namespace PKK\CoreBundle\Entity;

/**
 * Interface for Participation Entity.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
interface ParticipationInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id);

    /**
     * @return UserInterface
     */
    public function getUser();

    /**
     * @param UserInterface $user
     *
     * @return self
     */
    public function setUser(UserInterface $user);

    /**
     * @return CourseInterface
     */
    public function getCourse();

    /**
     * @param CourseInterface $course
     *
     * @return self
     */
    public function setCourse(CourseInterface $course);

    /**
     * @return boolean
     */
    public function getConfirmed();

    /**
     * @param boolean $confirmed
     *
     * @return self
     */
    public function setConfirmed($confirmed);
}
