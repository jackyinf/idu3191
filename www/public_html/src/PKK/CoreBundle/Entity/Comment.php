<?php

namespace PKK\CoreBundle\Entity;

use FOS\CommentBundle\Entity\Comment as BaseComment;

/**
 * Entity for comment.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class Comment extends BaseComment implements CommentInterface
{
    /**
     * Unique identifier.
     *
     * @var int
     */
    protected $id;

    /**
     * Author of the comment.
     *
     * @var UserInterface
     */
    protected $author;

    /**
     * Thread of this comment.
     *
     * @var CourseInterface
     */
    protected $course;

    /**
     * {@inheritdoc}
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * {@inheritdoc}
     */
    public function setAuthor(\Symfony\Component\Security\Core\User\UserInterface $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * {@inheritdoc}
     */
    public function setCourse(CourseInterface $course)
    {
        $this->course = $course;

        return $this;
    }
}
