<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * Interface for Course Entity.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
interface CourseInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description);

    /**
     * Get the date when course starts.
     *
     * @return \DateTime
     */
    public function getStart();

    /**
     * @param \DateTime $start
     *
     * @return self
     */
    public function setStart(\DateTime $start = null);

    /**
     * Get the date when course ends.
     *
     * @return \DateTime
     */
    public function getEnd();

    /**
     * Set the date when course ends.
     *
     * @param \DateTime $end
     *
     * @return self
     */
    public function setEnd(\DateTime $end = null);

    /**
     * Get the date when registration starts.
     *
     * @return \DateTime
     */
    public function getRegistrationStart();

    /**
     * Set the date when registration starts.
     *
     * @param \DateTime $registrationStart
     *
     * @return self
     */
    public function setRegistrationStart(\DateTime $registrationStart = null);

    /**
     * Get the date when registration ends.
     *
     * @return \DateTime
     */
    public function getRegistrationEnd();

    /**
     * Set the date when registration ends.
     *
     * @param \DateTime $registrationEnd
     *
     * @return self
     */
    public function setRegistrationEnd(\DateTime $registrationEnd = null);

    /**
     * @return UserInterface
     */
    public function getCreatedBy();

    /**
     * @param UserInterface $createdBy
     *
     * @return self
     */
    public function setCreatedBy(UserInterface $createdBy);

    /**
     * Get the maximum number of participants.
     *
     * @return int
     */
    public function getMaxParticipants();

    /**
     * Set the maximum number of participants.
     *
     * @param int $maxParticipants
     *
     * @return self
     */
    public function setMaxParticipants($maxParticipants);

    /**
     * @return Collection|ParticipationInterface[]
     */
    public function getParticipations();

    /**
     * @param Collection|ParticipationInterface[] $participations
     *
     * @return self
     */
    public function setParticipations(Collection $participations);

    /**
     * @return ParticipationInterface $participation
     *
     * @return self
     */
    public function addParticipation(ParticipationInterface $participation);

    /**
     * @param ParticipationInterface $participation
     *
     * @return self
     */
    public function removeParticipation(ParticipationInterface $participation);

    /**
     * @return string
     */
    public function getLocation();

    /**
     * @param string $location
     *
     * @return self
     */
    public function setLocation($location);

    /**
     * @return Collection|FeedbackTemplateInterface[]
     */
    public function getFeedbackTemplates();

    /**
     * @param Collection|FeedbackTemplateInterface[] $feedbackTemplates
     *
     * @return self
     */
    public function setFeedbackTemplates(Collection $feedbackTemplates);
}
