<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * Question type ('text', 'scale') in feedback.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackQuestionType implements FeedbackQuestionTypeInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * Type of the question group.
     *
     * @var string
     */
    protected $type;

    /**
     * Collection of all questions in this group.
     *
     * @var Collection|FeedbackQuestionInterface[]
     */
    protected $questions;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|FeedbackQuestionInterface[]
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param Collection|FeedbackQuestionInterface[] $questions
     *
     * @return self
     */
    public function setQuestions(Collection $questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (!empty($this->type)) ? $this->type : '';
    }
}
