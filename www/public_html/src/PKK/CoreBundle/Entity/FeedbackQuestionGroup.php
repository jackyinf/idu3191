<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Group of questions in feedback.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackQuestionGroup implements FeedbackQuestionGroupInterface
{
    use ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable,
        ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Translatable\Translatable;

    /**
     * Unique identifier.
     *
     * @var int
     */
    protected $id;

    /**
     * Collection of all questions in this group.
     *
     * @var Collection|FeedbackQuestion[]
     */
    protected $questions;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * {@inheritdoc}
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->getName() !== '') ? $this->getName() : 'Group';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        if ($this->getTranslations() !== null && $this->getTranslations()->count() > 0) {
            /** @var \PKK\CoreBundle\Entity\FeedbackQuestionGroupTranslation $translation */
            $translation = $this->translate($this->getCurrentLocale());
            $content = $translation->getName();

            return (!empty($content)) ? $content : $this->getTranslations()->first()->getName();
        }

        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function setTranslations(Collection $translations)
    {
        $this->translations = $translations;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addTranslation($translation)
    {
        $translation->setTranslatable($this);

        if ($translation->getLocale() === null) $translation->setLocale($this->getCurrentLocale());

        $this->getTranslations()->set($translation->getLocale(), $translation);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeTranslation($translation)
    {
        $translation->setTranslatable($this);

        $this->getTranslations()->removeElement($translation);

        return $this;
    }
}
