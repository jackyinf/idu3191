<?php

namespace PKK\CoreBundle\Entity;
use Doctrine\Common\Collections\Collection;

/**
 * Interface for FeedbackAnswer Entity.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
interface FeedbackAnswerInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $content
     *
     * @return self
     */
    public function setContent($content);

    /**
     * @return FeedbackQuestion
     */
    public function getQuestion();

    /**
     * @param FeedbackQuestion $question
     *
     * @return self
     */
    public function setQuestion(FeedbackQuestion $question);

    /**
     * @return Collection|FeedbackQuestionInterface[]
     */
    public function getQuestions();

    /**
     * @param Collection|FeedbackQuestionInterface[] $questions
     *
     * @return self
     */
    public function setQuestions(Collection $questions);
}
