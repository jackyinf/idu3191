<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Entity for user participation in courses.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class Participation implements ParticipationInterface
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     */
    private $id;

    /**
     * User, who participates in a course.
     *
     * @var UserInterface
     */
    private $user;

    /**
     * Course to participate in.
     *
     * @var CourseInterface
     */
    private $course;

    /**
     * Is participation confirmed by admin.
     *
     * @var boolean
     */
    private $confirmed;

    /**
     * Main constructor.
     */
    public function __construct()
    {
        $this->confirmed = false;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * {@inheritdoc}
     */
    public function setCourse(CourseInterface $course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * {@inheritdoc}
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (isset($this->user) && isset($this->course)) {
            return $this->user->getName() . ' (' . $this->user->getUsername() . ')';
        }

        return 'Participation';
    }
}
