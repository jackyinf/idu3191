<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Question answers in feedback.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackAnswer implements FeedbackAnswerInterface
{
    use ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * Unique identifier.
     *
     * @var int
     */
    protected $id;

    /**
     * Question content.
     *
     * @var string
     */
    protected $content;

    /**
     * Question to answer.
     *
     * @var FeedbackQuestion
     */
    protected $question;

    /**
     * Questions.
     *
     * @var Collection|FeedbackQuestionInterface[]
     */
    protected $questions;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * {@inheritdoc}
     */
    public function setQuestion(FeedbackQuestion $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * {@inheritdoc}
     */
    public function setQuestions(Collection $questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (!empty($this->content)) ? $this->content : 'Answer';
    }
}
