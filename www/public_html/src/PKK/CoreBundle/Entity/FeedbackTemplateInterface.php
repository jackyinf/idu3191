<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * Interface for FeedbackTemplate Entity.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
interface FeedbackTemplateInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * @return Collection|FeedbackQuestionInterface[]
     */
    public function getQuestions();

    /**
     * @param Collection|FeedbackQuestionInterface[] $questions
     *
     * @return self
     */
    public function setQuestions(Collection $questions);

    /**
     * @return Collection|CourseInterface[]
     */
    public function getCourses();

    /**
     * @param Collection|CourseInterface[] $courses
     *
     * @return self
     */
    public function setCourses(Collection $courses);
}
