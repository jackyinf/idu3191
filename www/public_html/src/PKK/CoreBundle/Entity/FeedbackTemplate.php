<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Template in feedback.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackTemplate implements FeedbackTemplateInterface
{
    use ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * Collection of all questions in this group.
     *
     * @var Collection|FeedbackQuestionInterface[]
     */
    protected $questions;

    /**
     * Collection of all courses in this group.
     *
     * @var Collection|CourseInterface[]
     */
    private $courses;

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * {@inheritdoc}
     */
    public function setQuestions(Collection $questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * {@inheritdoc}
     */
    public function setCourses(Collection $courses)
    {
        $this->courses = $courses;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addQuestion(FeedbackQuestionInterface $question)
    {
        $this->questions->add($question);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeTranslation(FeedbackQuestionInterface $question)
    {
        $this->questions->removeElement($question);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->getName() !== '') ? $this->getName() : 'Template';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
