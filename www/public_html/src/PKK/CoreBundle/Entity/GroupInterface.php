<?php

namespace PKK\CoreBundle\Entity;

/**
 * Interface for Group Entity.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
interface GroupInterface
{
    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description);

    /**
     * @return UserInterface
     */
    public function getUser();

    /**
     * @param UserInterface $user
     *
     * @return self
     */
    public function setUser(UserInterface $user);
}
