<?php

namespace PKK\CoreBundle\Entity;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Translation for question group in feedback.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class FeedbackQuestionGroupTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * Question group name.
     *
     * @var string
     */
    protected $name;

    /**
     * @return FeedbackQuestionGroup
     */
    public function getGroup()
    {
        return $this->getTranslatable();
    }

    /**
     * @param FeedbackQuestionGroup $group
     *
     * @return self
     */
    public function setGroup(FeedbackQuestionGroup $group)
    {
        $this->setTranslatable($group);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->getName() !== null) ? $this->getName() . ' (' . $this->getLocale() . ')' : 'Translation';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
