<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Sonata\UserBundle\Model\UserInterface as BaseUserInterface;

/**
 * Interface for User Entity.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
interface UserInterface extends BaseUserInterface
{
    /**
     * Returns the gender list
     *
     * @return array
     */
    public static function getGenderList();

    /**
     * Hook on pre-persist operations
     */
    public function prePersist();

    /**
     * Hook on pre-update operations
     */
    public function preUpdate();

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getGender();

    /**
     * @param string $gender
     *
     * @return self
     */
    public function setGender($gender);

    /**
     * @return string
     */
    public function getOrganization();

    /**
     * @param string $organization
     *
     * @return self
     */
    public function setOrganization($organization);

    /**
     * @return string
     */
    public function getPhone();

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone($phone);

    /**
     * @return Collection|CourseInterface[]
     */
    public function getCourses();

    /**
     * @param Collection|CourseInterface[] $courses
     *
     * @return self
     */
    public function setCourses(Collection $courses);

    /**
     * @return Collection|ParticipationInterface[]
     */
    public function getParticipations();

    /**
     * @param Collection|ParticipationInterface[] $participations
     *
     * @return self
     */
    public function setParticipations(Collection $participations);

    /**
     * @return Collection|CommentInterface[]
     */
    public function getComments();

    /**
     * @param Collection|CommentInterface[] $comments
     *
     * @return self
     */
    public function setComments(Collection $comments);
}
