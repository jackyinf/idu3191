<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * Interface for FeedbackQuestionType Entity.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
interface FeedbackQuestionTypeInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getType();

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType($type);

    /**
     * @return Collection|FeedbackQuestionInterface[]
     */
    public function getQuestions();

    /**
     * @param Collection|FeedbackQuestionInterface[] $questions
     *
     * @return self
     */
    public function setQuestions(Collection $questions);
}
