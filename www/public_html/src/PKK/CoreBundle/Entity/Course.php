<?php

namespace PKK\CoreBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Entity\Thread as BaseThread;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Entity for courses.
 *
 * @package PKK\CoreBundle\Entity
 *
 * @author  Victor Popkov <Dragon.vctr@gmail.com>
 */
class Course extends BaseThread implements CourseInterface
{
    use ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\Sluggable\Sluggable,
        ORMBehaviors\SoftDeletable\SoftDeletable,
        ORMBehaviors\Timestampable\Timestampable;

    /**
     * Unique identifier.
     *
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * Date when course starts.
     *
     * @var \DateTime
     */
    protected $start;

    /**
     * Date when course ends.
     *
     * @var \DateTime
     */
    protected $end;

    /**
     * Date when registration starts.
     *
     * @var \DateTime
     */
    protected $registrationStart;

    /**
     * Date when registration ends.
     *
     * @var \DateTime
     */
    protected $registrationEnd;

    /**
     * Maximum number of participants.
     *
     * @var int
     */
    protected $maxParticipants;

    /**
     * @var Collection|ParticipationInterface[]
     */
    protected $participations;

    /**
     * @var string
     */
    protected $location;

    /**
     * @var Collection|FeedbackTemplateInterface[]
     */
    protected $feedbackTemplates;

    /**
     * Main constructor.
     */
    public function __construct()
    {
        $this->start = new \DateTime();
        $this->registrationStart = new \DateTime();
        $this->isCommentable = true;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * {@inheritdoc}
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * {@inheritdoc}
     */
    public function setStart(\DateTime $start = null)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * {@inheritdoc}
     */
    public function setEnd(\DateTime $end = null)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRegistrationStart()
    {
        return $this->registrationStart;
    }

    /**
     * {@inheritdoc}
     */
    public function setRegistrationStart(\DateTime $registrationStart = null)
    {
        $this->registrationStart = $registrationStart;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRegistrationEnd()
    {
        return $this->registrationEnd;
    }

    /**
     * {@inheritdoc}
     */
    public function setRegistrationEnd(\DateTime $registrationEnd = null)
    {
        $this->registrationEnd = $registrationEnd;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedBy(UserInterface $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * {@inheritdoc}
     */
    public function getMaxParticipants()
    {
        return $this->maxParticipants;
    }

    /**
     * {@inheritdoc}
     */
    public function setMaxParticipants($maxParticipants)
    {
        $this->maxParticipants = $maxParticipants;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParticipations()
    {
        return $this->participations;
    }

    /**
     * {@inheritdoc}
     */
    public function setParticipations(Collection $participations)
    {
        $this->participations = $participations;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * {@inheritdoc}
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSluggableFields()
    {
        return ['name'];
    }

    /**
     * {@inheritdoc}
     */
    public function generateSlugValue($values)
    {
        return implode('-', $values);
    }

    /**
     * {@inheritdoc}
     */
    public function getFeedbackTemplates()
    {
        return $this->feedbackTemplates;
    }

    /**
     * {@inheritdoc}
     */
    public function setFeedbackTemplates(Collection $feedbackTemplates)
    {
        $this->feedbackTemplates = $feedbackTemplates;

        return $this;
    }

    /**
     * Generates and sets the entity's slug and then set permalink.
     * Called prePersist and preUpdate.
     */
    public function generateSlugAndPermalink()
    {
        $this->generateSlug();
        $this->permalink = $this->getSlug();
    }

    /**
     * {@inheritdoc}
     */
    public function addParticipation(ParticipationInterface $participation)
    {
        $participation
            ->setCourse($this)
            ->setConfirmed(true);

        $this->participations->add($participation);
    }

    /**
     * {@inheritdoc}
     */
    public function removeParticipation(ParticipationInterface $participation)
    {
        $this->participations->removeElement($participation);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (!empty($this->name)) ? $this->name : 'Course';
    }
}
