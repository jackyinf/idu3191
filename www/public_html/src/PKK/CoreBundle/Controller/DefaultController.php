<?php

namespace PKK\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('PKKCoreBundle:Static:index.html.twig', array('name' => $name));
    }
}
