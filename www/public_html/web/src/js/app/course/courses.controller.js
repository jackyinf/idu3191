/**
 * Courses Controller.
 *
 * @version    1.0.0
 * @author     Jevgeni Rumjantsev <zeka.rum@gmail.com>
 * @created    17.03.2015
 * @modified   26.03.2015
 */

'use strict';

define(function (app) {

    var coursesController = function ($scope, $rootScope, $state, toastr, courseService, userService) {
        $scope.vm = this;
        $scope.vm.goToCourse = goToCourse;
        $scope.vm.registerToCourse = registerToCourse;
        $scope.vm.cancelRegistration = cancelRegistration;
        this.goToFeedbackForm = goToFeedbackForm;

        activate();

        //////////////////////////

        function goToCourse(id) {
            $state.go('course-read', { id: id });
        }

        function goToFeedbackForm(id) {
            $state.go('feedback-read', { id: id });
        }

        function registerToCourse(course) {
            courseService.register(course.id)
                .success(function (data, status) {
                    if (status === 200) {
                        toastr.success('Registered successfully');
                        $scope.vm.current_user_is_registered = true;
                        course.registered = true;
                    }
                }).error(function (data, status) {
                    if (status === 400) {
                        toastr.error('You are already participating in this course');
                    }
                });
        }

        function cancelRegistration(course) {
            courseService.cancelRegistration(course.id)
                .success(function (data, status) {
                    if (status === 204) {
                        toastr.info('Successfully unregistered');
                        $scope.vm.current_user_is_registered = false;
                        course.registered = false;
                    }
                }).error(function (data, status) {
                    if (status === 401) {
                        toastr.error('You are not authorized');
                    } else if (status === 404) {
                        toastr.error('You are not registered to current course');
                    }
                });
        }

        function activate() {
            if ($rootScope.loggedIn !== true) {
                userService.getMe().success(function (data, status) {
                    if (status === 200) {
                        toastr.success('Tere tulemast, ' + data.name + '!');
                        userService.greeted = true;
                        $rootScope.loggedIn = true;
                    } else if (status === 401) {
                        $rootScope.loggedIn = false;
                    }
                });
            }

            userService.getMe().success(function (data, status) {
                if (status === 200) {
                    $scope.me = data;
                }
            });

            courseService.getAll().success(function (data) {
                if ($rootScope.loggedIn) {
                    for (var i = 0; i < data.length; i++) {
                        for (var j = 0; j < data[i].participations.length; j++) {
                            if (data[i].participations[j].user.id === $scope.me.id) {
                                data[i].registered = true;
                                break;
                            }
                        }
                    }
                }
                $scope.vm.courses = data;
            });
        }
    };

    coursesController.$inject = ['$scope', '$rootScope', '$state', 'toastr', 'course.service', 'user.service'];

    return coursesController;

});
