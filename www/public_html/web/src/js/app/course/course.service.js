/**
 * Course Service.
 *
 * @version    1.0.0
 * @author     Jevgeni Rumjantsev <zeka.rum@gmail.com>
 * @created    17.03.2015
 * @modified   26.03.2015
 */

'use strict';

define(function () {

    var courseService = function ($http) {
        var service = {
            getAll: getAll,
            getAllEnded: getAllEnded,
            getSingle: getSingle,
            register: register,
            cancelRegistration: cancelRegistration,
            getTopUpcoming: getTopUpcoming,
            getTopEnded: getTopEnded
        };

        return service;

        ///////////////////////////////////

        function getAll() {
            return $http.get(Routing.generate('pkk_api_course_all', { _format: 'json' }));
        }

        function getAllEnded() {
            return $http.get(Routing.generate('pkk_api_course_all_ended', { _format: 'json' }));
        }

        function getSingle(id) {
            return $http.get(Routing.generate('pkk_api_course_get', { _format: 'json', id: id }));
        }

        function register(id) {
            return $http.post(Routing.generate('pkk_api_course_register', { _format: 'json', id: id }));
        }

        function cancelRegistration(id) {
            return $http.delete(Routing.generate('pkk_api_course_cancel', { _format: 'json', id: id }));
        }

        function getTopUpcoming() {
            return getAll().success(function (data) {
                return data.slice(0, 4);
            });
        }

        function getTopEnded() {
            return getAllEnded().success(function (data) {
                return data.slice(0, 4);
            });
        }
    };

    courseService.$inject = ['$http'];

    return courseService;

});
