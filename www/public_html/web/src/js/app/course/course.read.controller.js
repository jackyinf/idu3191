/**
 * Course reading Controller.
 *
 * @version    1.0.0
 * @author     Jevgeni Rumjantsev <zeka.rum@gmail.com>
 * @created    17.03.2015
 * @modified   26.03.2015
 */

'use strict';

define(function () {

    var courseReadController = function ($scope, $q, $state, $stateParams, toastr, courseService, userService) {

        $scope.vm = this;

        $scope.vm.registerToCourse = registerToCourse;
        $scope.vm.cancelRegistration = cancelRegistration;
        $scope.vm.goBack = goBack;

        $scope.vm.ready = false;
        $scope.me = null;

        activate();

        //////////////////////////

        function registerToCourse() {
            courseService.register($stateParams.id)
                .success(function (data, status) {
                    if (status === 200) {
                        toastr.success('Registered successfully');
                        $scope.vm.current_user_is_registered = true;
                    }
                }).error(function (data, status) {
                    if (status === 400) {
                        toastr.error('You are already participating in this course');
                    }
                });
        }

        function cancelRegistration() {
            courseService.cancelRegistration($stateParams.id)
                .success(function (data, status) {
                    if (status === 204) {
                        toastr.info('Successfully unregistered');
                        $scope.vm.current_user_is_registered = false;
                    }
                }).error(function (data, status) {
                    if (status === 401) {
                        toastr.error('You are not authorized');
                    } else if (status === 404) {
                        toastr.error('You are not registered to current course');
                    }
                });
        }

        function goBack() {
            $state.go('courses');
        }

        function activate() {
            // Get current user, if logged in
            var userPromise = userService.getMe().success(function (data, status) {
                if (status === 200) {
                    $scope.me = data;
                }
            }).error(function (data, status) {
                if (status === 401) {
                    toastr.info('Please login before subscribing to the course');
                }
            });

            // Get current course
            var coursePromise = courseService.getSingle($stateParams.id).success(function (course) {
                $scope.vm.course = course;
                if (course && course.participation) {
                    $scope.vm.course.registered_participants = course.participation.length;
                }
            }).error(function (err) {
                toastr.error(err);
            });

            // When finding current course and current user has been finished
            $scope.myPromise = $q.all([userPromise, coursePromise]).then(function () {
                if ($scope.me && $scope.vm && $scope.vm.course) {
                    // Get all participants
                    var participations = $scope.vm.course.participations;

                    // Check, if user is registered
                    for (var i = 0; i < participations.length; i++) {
                        if (participations[i].user.id === $scope.me.id) {
                            $scope.vm.current_user_is_registered = true;
                            break;
                        }
                    }
                }
                $scope.vm.ready = true;
            });
        }
    };

    courseReadController.$inject = ['$scope', '$q', '$state', '$stateParams', 'toastr', 'course.service', 'user.service'];

    return courseReadController;

});
