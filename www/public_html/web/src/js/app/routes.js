/**
 * The AngularJS app routes.
 *
 * @version    1.0.0
 * @author     Jevgeni Rumjantsev <zeka.rum@gmail.com>
 * @author     Victor Popkov <Dragon.vctr@gmail.com>
 * @created    22.02.2015
 * @modified   26.03.2015
 */

'use strict';

define(function () {

    var routeConfig = function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: Routing.generate('pkk_front_partial', { 'name': 'mainpage' }),
                controller: 'main.controller'
            })
            .state('me', {
                url: '/me',
                templateUrl: Routing.generate('pkk_front_partial', { 'name': 'me' }),
                controller: 'user.controller'
            })
            .state('contact', {
                url: '/contact',
                templateUrl: Routing.generate('pkk_front_partial', { 'name': 'contact-us' })
            })
            .state('about', {
                url: '/about',
                templateUrl: Routing.generate('pkk_front_partial', { 'name': 'about-us' })
            })
            .state('course-read', {
                url: '/course/:id',
                templateUrl: Routing.generate('pkk_front_partial', { 'name': 'course-read' }),
                controller: 'course.read.controller'
            })
            .state('courses', {
                url: '/courses',
                templateUrl: Routing.generate('pkk_front_partial', { 'name': 'courses' }),
                controller: 'courses.controller'
            })
            .state('feedback-read', {
                url: '/feedback/:id',
                templateUrl: Routing.generate('pkk_front_partial', { 'name': 'feedback-read' }),
                controller: 'feedback.read.controller'
            });
    };

    return routeConfig;

});
