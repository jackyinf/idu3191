/**
 * User reading Controller.
 *
 * @version    1.0.0
 * @author     Jevgeni Rumjantsev <zeka.rum@gmail.com>
 * @created    03.04.2015
 * @modified   03.04.2015
 */

'use strict';

define(function () {

    var userController = function ($scope, $state, toastr, userService) {

        $scope.vm = this;
        $scope.vm.goBack = goBack;

        activate();

        //////////////////////////

        function goBack() {
            $state.go('courses');
        }

        function activate() {
            userService.getMe().success(function (me) {
                $scope.vm.me = me;
            }).error(function (err) {
                toastr.error(err);
            });
        }
    };

    userController.$inject = ['$scope', '$state', 'toastr', 'user.service'];

    return userController;

});
