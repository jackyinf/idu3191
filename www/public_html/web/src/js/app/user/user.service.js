/**
 * User Service.
 *
 * @version    1.0.0
 * @author     Jevgeni Rumjantsev <zeka.rum@gmail.com>
 * @created    03.04.2015
 * @modified   03.04.2015
 */

'use strict';

define(function () {

    var userService = function ($http) {
        var service = {
            getSingle: getSingle,
            getMe: getMe,

            greeted: false
        };

        return service;

        ///////////////////////////////////

        function getSingle(id) {
            return $http.get(Routing.generate('pkk_api_user_get', { id: id }));
        }

        function getMe() {
            return $http.get(Routing.generate('pkk_api_user_get_me'));
        }
    };

    userService.$inject = ['$http'];

    return userService;

});
