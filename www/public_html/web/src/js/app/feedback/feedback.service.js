/**
 * Feedback Service.
 *
 * @version    1.0.0
 * @author     Jevgeni Rumjantsev <zeka.rum@gmail.com>
 * @created    17.03.2015
 * @modified   26.03.2015
 */

'use strict';

define(function () {

    var feedbackService = function($http) {
        var service = {
            getSingle: getSingle,
            submitFeedback: submitFeedback
        };

        return service;

        ///////////////////////////////////

        function getSingle(id) {
            return $http.get('/api/v1/feedback-template/:id', { id: id });
        }

        function submitFeedback(courseId, answers) {
            return $http.post('/api/v1/feedback/submit', { courseId: courseId, answers: answers });
        }
    };

    feedbackService.$inject = ['$http'];

    return feedbackService;

});
