/**
 * Feedback reading Controller.
 *
 * @version    1.0.0
 * @author     Jevgeni Rumjantsev <zeka.rum@gmail.com>
 * @created    17.03.2015
 * @modified   26.03.2015
 */

'use strict';

define(function () {

    var feedbackReadController = function ($scope, $stateParams, feedbackService) {

        $scope.vm = this;
        $scope.vm.submitFeedback = submitFeedback;
        $scope.vm.goBack = goBack;

        activate();

        //////////////////////////

        function submitFeedback() {
            // (good old for-loop) :) todo: would be cleaner with sugar.js
            var answers = [];
            for (var i = 0; i < $scope.vm.feedback.question_groups.length; i++) {
                var group = $scope.vm.feedback.question_groups[i];
                for (var j = 0; j < group.questions.length; j++) {
                    var question = group.questions[j];
                    answers.push({ question_id: question.id, answer: question.answer });
                }
            }

            console.log(answers);           // todo: temporary. to remove
            return;                         // todo: temporary. to remove

            //feedbackService.submitFeedback(answers)
            //    .success(function () {
            //        $scope.vm.goBack();
            //    });
            //promise.error(function () {
            //
            //});
        }

        function goBack() {
            location.href = "#/courses";
        }

        function activate() {

            // todo: temporary sample feedback structure. to remove
            $scope.vm.feedback = {
                question_groups: [
                    // Group 1
                    {
                        name: 'Peaküsimused',
                        questions: [
                            { id: 1, text: 'Kas teile meeldis kursus?', type: 'string', answer: '' },
                            {
                                id: 2,
                                text: 'Kui palju teile meeldis kursus?',
                                type: 'from_five_to_five',
                                answer: '',
                                options: [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]
                            }
                        ]
                    },

                    // Group 2
                    {
                        name: 'Lisaküsimused',
                        questions: [
                            { id: 3, text: 'Mida tahaksite veel lisada?', type: 'string', answer: '' }
                        ]
                    }
                ]
            };

            return;                         // todo. temporary. to remove

            //feedbackService.getSingle($stateParams.id).success(function (feedback) {
            //    $scope.vm.feedback = feedback;
            //}).error(function (err) {
            //    //console.log(err);
            //});

        }
    };

    //app.controller('feedback.read.controller', feedbackReadController);

    feedbackReadController.$inject = ['$scope', '$stateParams', 'feedback.service'];

    return feedbackReadController;

});
