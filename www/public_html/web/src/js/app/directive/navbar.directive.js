/**
 * Navigation bar directive.
 *
 * @version    1.0.0
 * @author     Jevgeni Rumjantsev <zeka.rum@gmail.com>
 * @created    17.03.2015
 * @modified   26.03.2015
 */

'use strict';

define(function () {
    var navbar = function (userService) {
        return {
            templateUrl: Routing.generate('pkk_front_partial', { 'name': 'navbar' }),
            link: function ($scope, element) {
                $scope.ready = false;
                userService.getMe().success(function (me) {
                    $scope.me = me;
                }).finally(function () {
                    $scope.ready = true;
                });
            }
        };
    };

    navbar.$inject = ['user.service'];

    return navbar;
});
