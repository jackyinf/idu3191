/**
 * The AngularJS app initialization.
 *
 * @version    1.0.0
 * @author     Jevgeni Rumjantsev <zeka.rum@gmail.com>
 * @author     Victor Popkov <Dragon.vctr@gmail.com>
 * @created    22.02.2015
 * @modified   26.03.2015
 */

'use strict';

define(['use!angular',
        'angularAMD',
        'angularUiRouter',
        'angular-route',
        'uiBootstrap',
        'toastr',
        //'ngFxBundle',
        //'ngFx',
        //'ngBusy',
        //'sugar',
        'underscore',

        // Config
        'app/routes',

        // Controllers
        'app/user/user.controller',
        'app/main/main.controller',
        'app/course/courses.controller',
        'app/course/course.read.controller',
        'app/feedback/feedback.read.controller',

        // Services
        'app/user/user.service',
        'app/course/course.service',
        'app/feedback/feedback.service',

        // Directives
        'app/directive/navbar.directive'
    ],
    function (angular,
              angularAMD,
              angularUiRouter,
              angularRoute,
              uiBootstrap,
              toastr,
              //ngFxBundle,
              //ngFx,
              //ngBusy,
              //sugar,
              underscore,
              // Config
              routeConfig,
              // Controllers
              userController,
              mainController,
              coursesController,
              courseReadController,
              feedbackReadController,
              // Services
              userService,
              courseService,
              feedbackService,
              // Directives
              navbar) {
        /* Create angular application module (a.k.a. main module). */
        var app = angular.module('webApp', ['ui.router', 'ui.bootstrap', 'toastr']); // 'ngFx', 'cgBusy'

        /*
         * Define everything here before launching an app.
         * Order matters. Define services first and then controllers because controllers depend on services.
         */
        app.service('user.service', userService);
        app.service('course.service', courseService);
        app.service('feedback.service', feedbackService);

        app.controller('user.controller', userController);
        app.controller('main.controller', mainController);
        app.controller('courses.controller', coursesController);
        app.controller('course.read.controller', courseReadController);
        app.controller('feedback.read.controller', feedbackReadController);

        app.directive('navbar', navbar);

        app.filter('pkkDateFormat', function myDateFormat($filter) {
            return function (text) {
                var tempdate = new Date(text);
                return $filter('date')(tempdate, "dd. MMMM, yyyy");
            };
        });

        /* Enable navigation using ui.router library */
        app.config(routeConfig);

        app.config(function ($interpolateProvider, $locationProvider) {

            /*
             *  Interpolation rule: you can write angular variables in HTML like this -> {[{ hello }]}.
             *  Default rule is {{ hello }}.
             */
            $interpolateProvider.startSymbol('{[{').endSymbol('}]}');

            $locationProvider.html5Mode(true).hashPrefix('!');
        });

        /* Disable caching of the views */
        app.run(function ($rootScope, $templateCache) {
            $rootScope.$on('$viewContentLoaded', function () {
                $templateCache.removeAll();
                $templateCache.put("templates/toastr/toastr.html", "<div class=\"{{toastClass}} {{toastType}}\" ng-click=\"tapToast()\">\n  <div ng-switch on=\"allowHtml\">\n    <div ng-switch-default ng-if=\"title\" class=\"{{titleClass}}\">{{title}}</div>\n    <div ng-switch-default class=\"{{messageClass}}\">{{message}}</div>\n    <div ng-switch-when=\"true\" ng-if=\"title\" class=\"{{titleClass}}\" ng-bind-html=\"title\"></div>\n    <div ng-switch-when=\"true\" class=\"{{messageClass}}\" ng-bind-html=\"message\"></div>\n  </div>\n</div>");
            });
        });

        /* This is requirejs-like alternative to usage of ng-app in HTML. Do not use ng-app when using AMD bootstrapping. */
        angular.element(document).ready(function () {
            angularAMD.bootstrap(app);
        });

        return app;
    });
