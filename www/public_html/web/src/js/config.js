/**
 * The RequireJS main config file and libraries initialization.
 *
 * @version    1.0.0
 * @author     Victor Popkov <Dragon.vctr@gmail.com>
 * @created    28.02.2015
 * @modified   26.03.2015
 */

'use strict';

require.config({
    urlArgs: 'bust=' + (new Date()).getTime(),
    paths: {
        /* Modernizr */
        'modernizr': [
            '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min',
            'vendor/modernizr.min'
        ],

        /* AngularJS */
        'angular': [
            '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular',
            'vendor/angular/angular'
        ],

        'angularUiRouter': [
            '//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.13/angular-ui-router',
            'vendor/angular/angular-ui-router'
        ],

        'angular-route': 'vendor/angular/angular-route',

        'uiBootstrap': [
            '//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.12.1/ui-bootstrap-tpls',
            'vendor/angular/ui-bootstrap-tpls'
        ],

        'toastr': 'vendor/angular/angular-toastr.tpls',

        /* other vendor scripts */
        'angularAMD': 'vendor/angular/angularAMD.min',
        'ngload': 'vendor/angular/ngload',
        //'ngFxBundle': 'vendor/angular/ngFxBundle',
        //'ngFx': 'vendor/angular/ngFx',
        //'ngBusy': 'vendor/angular/ngBusy',
        //'sugar': 'vendor/sugar.min',
        'underscore': 'vendor/underscore',
        'use': 'vendor/use'
    },
    shim: {
        'angularAMD': ['angular'],
        'angular-route': ['angular'],
        'angularUiRouter': ['angular'],
        'uiBootstrap': ['angular'],
        //'ngFxBundle': ['angular'],
        //'ngFx': ['angular', 'ngFxBundle'],
        'toastr': ['angular']
    },
    use: {
        'modernizr': {
            attach: 'Modernizr'
        },
        'angular': {
            attach: 'angular'
        },
        'angular-route': {
            deps: ['use!angular']
        },
        'angularUiRouter': {
            deps: ['use!angular']
        },
        'uiBootstrap': {
            deps: ['use!angular']
        },
        'toastr': {
            deps: ['use!angular']
        },
        'angularAMD': {
            deps: ['use!angular']
        }
        //'ngFx': {
        //    deps: ['use!ngFxBundle']
        //},
        //'ngFxBundle': {
        //    deps: ['use!angular']
        //},
        //'ngBusy': {
        //    deps: ['use!angular']
        //}
    },
    deps: ['app']
});
