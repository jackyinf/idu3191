# ************************************************************
# Sequel Pro SQL dump
# Версия 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Адрес: 127.0.0.1 (MySQL 5.5.41-0ubuntu0.14.04.1)
# Схема: pkk_db
# Время создания: 2015-04-12 15:42:38 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ancestors` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `depth` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526C591CC992` (`course_id`),
  KEY `IDX_9474526CA76ED395` (`user_id`),
  CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_9474526C591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;

INSERT INTO `comment` (`id`, `course_id`, `user_id`, `body`, `ancestors`, `depth`, `created_at`, `state`)
VALUES
  (1,1,3,'\"I volunteer as tribute!\" — why did I say that? So dumb! How can I cancel my participation in this event?','',0,'2015-04-12 18:42:28',0),
  (2,1,2,'You can\'t. But don\'t worry, I will send you some roses to cheer you up!','1',1,'2015-04-12 18:42:28',0),
  (3,1,4,'Why me? I don\'t want to go either! I can\'t find that \"Cancel\" button…','',0,'2015-04-12 18:42:28',0),
  (4,1,1,'Am I in the wrong story? Well, I\'m Captain Jack Sparrow, so it\'s not a surprise that I rule even in this ocean. Savvy?','',0,'2015-04-12 18:42:28',0);

/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы course
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course`;

CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_commentable` tinyint(1) NOT NULL,
  `num_comments` int(11) NOT NULL,
  `last_comment_at` datetime DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `start` date NOT NULL,
  `end` date DEFAULT NULL,
  `registration_start` date NOT NULL,
  `registration_end` date DEFAULT NULL,
  `max_participants` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_169E6FB9DE12AB56` (`created_by`),
  KEY `IDX_169E6FB916FE72E1` (`updated_by`),
  KEY `IDX_169E6FB91F6FA0AF` (`deleted_by`),
  CONSTRAINT `FK_169E6FB91F6FA0AF` FOREIGN KEY (`deleted_by`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_169E6FB916FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_169E6FB9DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;

INSERT INTO `course` (`id`, `created_by`, `updated_by`, `deleted_by`, `permalink`, `is_commentable`, `num_comments`, `last_comment_at`, `name`, `description`, `start`, `end`, `registration_start`, `registration_end`, `max_participants`, `location`, `created_at`, `updated_at`, `deleted_at`, `slug`)
VALUES
  (1,2,NULL,NULL,'hunger-games',1,0,NULL,'Hunger Games','A morbid and brutal competition which takes place annually in the country of Panem. Every year, one boy and one girl between the ages of 12 and 18 were selected from each of the twelve districts as tributes, who train for a week to be gladiators and then are sent into an outdoor arena to fight to the death.','2015-04-12',NULL,'2015-04-12',NULL,24,'Clock Arena','2015-04-12 18:42:27','2015-04-12 18:42:27',NULL,'hunger-games');

/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы feedback_answer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback_answer`;

CREATE TABLE `feedback_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E7917E6B1E27F6BF` (`question_id`),
  KEY `IDX_E7917E6BDE12AB56` (`created_by`),
  KEY `IDX_E7917E6B16FE72E1` (`updated_by`),
  KEY `IDX_E7917E6B1F6FA0AF` (`deleted_by`),
  CONSTRAINT `FK_E7917E6B1F6FA0AF` FOREIGN KEY (`deleted_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_E7917E6B16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_E7917E6B1E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `feedback_question` (`id`),
  CONSTRAINT `FK_E7917E6BDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Дамп таблицы feedback_question
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback_question`;

CREATE TABLE `feedback_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F9B9E23CC54C8C93` (`type_id`),
  KEY `IDX_F9B9E23CFE54D947` (`group_id`),
  KEY `IDX_F9B9E23CDE12AB56` (`created_by`),
  KEY `IDX_F9B9E23C16FE72E1` (`updated_by`),
  KEY `IDX_F9B9E23C1F6FA0AF` (`deleted_by`),
  CONSTRAINT `FK_F9B9E23C1F6FA0AF` FOREIGN KEY (`deleted_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_F9B9E23C16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_F9B9E23CC54C8C93` FOREIGN KEY (`type_id`) REFERENCES `feedback_question_type` (`id`),
  CONSTRAINT `FK_F9B9E23CDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_F9B9E23CFE54D947` FOREIGN KEY (`group_id`) REFERENCES `feedback_question_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `feedback_question` WRITE;
/*!40000 ALTER TABLE `feedback_question` DISABLE KEYS */;

INSERT INTO `feedback_question` (`id`, `type_id`, `group_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
  (1,1,1,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (2,1,1,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (3,2,2,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (4,2,2,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (5,2,2,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (6,2,3,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (7,2,3,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (8,2,3,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (9,2,3,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (10,2,4,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (11,2,4,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (12,2,4,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (13,1,4,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (14,1,5,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (15,1,5,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL);

/*!40000 ALTER TABLE `feedback_question` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы feedback_question_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback_question_group`;

CREATE TABLE `feedback_question_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1C3BB27FDE12AB56` (`created_by`),
  KEY `IDX_1C3BB27F16FE72E1` (`updated_by`),
  KEY `IDX_1C3BB27F1F6FA0AF` (`deleted_by`),
  CONSTRAINT `FK_1C3BB27F1F6FA0AF` FOREIGN KEY (`deleted_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_1C3BB27F16FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_1C3BB27FDE12AB56` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `feedback_question_group` WRITE;
/*!40000 ALTER TABLE `feedback_question_group` DISABLE KEYS */;

INSERT INTO `feedback_question_group` (`id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
  (1,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (2,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (3,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (4,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL),
  (5,2,NULL,NULL,'2015-04-12 18:42:28','2015-04-12 18:42:28',NULL);

/*!40000 ALTER TABLE `feedback_question_group` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы feedback_question_group_translation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback_question_group_translation`;

CREATE TABLE `feedback_question_group_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `feedback_question_group_translation_unique_translation` (`translatable_id`,`locale`),
  KEY `IDX_610AE6FC2C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_610AE6FC2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `feedback_question_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `feedback_question_group_translation` WRITE;
/*!40000 ALTER TABLE `feedback_question_group_translation` DISABLE KEYS */;

INSERT INTO `feedback_question_group_translation` (`id`, `translatable_id`, `name`, `locale`)
VALUES
  (1,1,'Данные о курсах','ru'),
  (2,2,'Общая оценка курсам','ru'),
  (3,3,'Содержание курсов','ru'),
  (4,4,'Общетехническая организация','ru'),
  (5,5,'Фоновые данные','ru');

/*!40000 ALTER TABLE `feedback_question_group_translation` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы feedback_question_translation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback_question_translation`;

CREATE TABLE `feedback_question_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `feedback_question_translation_unique_translation` (`translatable_id`,`locale`),
  KEY `IDX_CC1843402C2AC5D3` (`translatable_id`),
  CONSTRAINT `FK_CC1843402C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `feedback_question` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `feedback_question_translation` WRITE;
/*!40000 ALTER TABLE `feedback_question_translation` DISABLE KEYS */;

INSERT INTO `feedback_question_translation` (`id`, `translatable_id`, `content`, `locale`)
VALUES
  (1,1,'Наименование курсов','ru'),
  (2,2,'Лекторы','ru'),
  (3,3,'Как Вы удовлетворены проведенными курсами?','ru'),
  (4,4,'Как помогло участие в курсах совершенствовать Ваши профессиональные умения?','ru'),
  (5,5,'Как Вы удовлетворены атмосферой в группе во время курсов?','ru'),
  (6,6,'Как Вы удовлетворены  знаниями, полученными в ходе работы в аудитории?','ru'),
  (7,7,'Как Вы оцениваете профессиональность прохождения тем курсов лектором?','ru'),
  (8,8,'Как Вы оцениваете понятливость представления тем лектором?','ru'),
  (9,9,'Как Вы оцениваете умение лектора  общаться с аудиторией?','ru'),
  (10,10,'Как оцениваете материал (слайды, копии и др.) курсов?','ru'),
  (11,11,'Как оцениваете технические средства курсов?','ru'),
  (12,12,'Как оцениваете помещения обучения?','ru'),
  (13,13,'Ваши предложения и комментарии по курсам в том числе в какой области пожелали бы в будущем дополнительное обучение','ru'),
  (14,14,'Дата заполнения листка обратной связи','ru'),
  (15,15,'Пол (М/Ж)','ru');

/*!40000 ALTER TABLE `feedback_question_translation` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы feedback_question_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback_question_type`;

CREATE TABLE `feedback_question_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `feedback_question_type` WRITE;
/*!40000 ALTER TABLE `feedback_question_type` DISABLE KEYS */;

INSERT INTO `feedback_question_type` (`id`, `type`)
VALUES
  (1,'text'),
  (2,'scale');

/*!40000 ALTER TABLE `feedback_question_type` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы feedback_template
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback_template`;

CREATE TABLE `feedback_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D82EB4F1591CC992` (`course_id`),
  KEY `IDX_D82EB4F1DE12AB56` (`created_by`),
  KEY `IDX_D82EB4F116FE72E1` (`updated_by`),
  KEY `IDX_D82EB4F11F6FA0AF` (`deleted_by`),
  CONSTRAINT `FK_D82EB4F11F6FA0AF` FOREIGN KEY (`deleted_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_D82EB4F116FE72E1` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_D82EB4F1591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FK_D82EB4F1DE12AB56` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `feedback_template` WRITE;
/*!40000 ALTER TABLE `feedback_template` DISABLE KEYS */;

INSERT INTO `feedback_template` (`id`, `course_id`, `created_by`, `updated_by`, `deleted_by`, `name`, `created_at`, `updated_at`, `deleted_at`)
VALUES
  (1,1,NULL,NULL,NULL,'Default','2015-04-12 18:42:28','2015-04-12 18:42:28',NULL);

/*!40000 ALTER TABLE `feedback_template` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы feedback_template_question
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedback_template_question`;

CREATE TABLE `feedback_template_question` (
  `template_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`template_id`,`question_id`),
  UNIQUE KEY `UNIQ_2AE9DF291E27F6BF` (`question_id`),
  KEY `IDX_2AE9DF295DA0FB8` (`template_id`),
  CONSTRAINT `FK_2AE9DF291E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `feedback_question` (`id`),
  CONSTRAINT `FK_2AE9DF295DA0FB8` FOREIGN KEY (`template_id`) REFERENCES `feedback_template` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `feedback_template_question` WRITE;
/*!40000 ALTER TABLE `feedback_template_question` DISABLE KEYS */;

INSERT INTO `feedback_template_question` (`template_id`, `question_id`)
VALUES
  (1,1),
  (1,2),
  (1,3),
  (1,4),
  (1,5),
  (1,6),
  (1,7),
  (1,8),
  (1,9),
  (1,10),
  (1,11),
  (1,12),
  (1,13),
  (1,14),
  (1,15);

/*!40000 ALTER TABLE `feedback_template_question` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы group_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_role`;

CREATE TABLE `group_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7E33D11A5E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `group_role` WRITE;
/*!40000 ALTER TABLE `group_role` DISABLE KEYS */;

INSERT INTO `group_role` (`id`, `name`, `roles`, `description`)
VALUES
  (1,'Administrator','a:2:{i:0;s:16:\"ROLE_SUPER_ADMIN\";i:1;s:17:\"ROLE_SONATA_ADMIN\";}','Administrator has access to administration panel and can manage everything'),
  (2,'Participant','a:1:{i:0;s:16:\"ROLE_PARTICIPANT\";}','Participant can register to events');

/*!40000 ALTER TABLE `group_role` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы participation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `participation`;

CREATE TABLE `participation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_confirmed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AB55E24FA76ED395` (`user_id`),
  KEY `IDX_AB55E24F591CC992` (`course_id`),
  CONSTRAINT `FK_AB55E24F591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FK_AB55E24FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `participation` WRITE;
/*!40000 ALTER TABLE `participation` DISABLE KEYS */;

INSERT INTO `participation` (`id`, `user_id`, `course_id`, `created_at`, `updated_at`, `is_confirmed`)
VALUES
  (1,3,1,'2015-04-12 18:42:28','2015-04-12 18:42:28',1),
  (2,4,1,'2015-04-12 18:42:28','2015-04-12 18:42:28',1);

/*!40000 ALTER TABLE `participation` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `organization` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `name`, `gender`, `timezone`, `phone`, `organization`, `created_at`, `updated_at`)
VALUES
  (1,'Jack','jack','jack.sparrow@example.com','jack.sparrow@example.com',1,'dmutzah1q4g0wggk8ww48wkgsg44g8s','q24nZOFy70FHSUDyb2ZsDjwHXv22YG2vRcI3VFVa9Bi32blWn/nTxRHts83A7jGKDPiwn7xSBDKNzrwYdhc4jQ==',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'Jack Sparrow','m',NULL,'55666666','Black Pearl OÜ','2015-04-12 18:42:27','2015-04-12 18:42:27'),
  (2,'Snow','snow','Coriolanus.Snow@example.com','coriolanus.snow@example.com',1,'2n6pblni2p6o0ooo8kcsgo0s0w8gc4c','aDr5Mh6dr5qkO/faCpVE9BnxmMg5zZHS6J4otEu1DCaJ9ZkMO9gSJmBw58Xf5M4cxLboHPejiltojwAUZppaXQ==',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'President Snow','m',NULL,'55666333','The Capitol OÜ','2015-04-12 18:42:27','2015-04-12 18:42:27'),
  (3,'Katniss','katniss','Katniss.Everdeen@example.com','katniss.everdeen@example.com',1,'6hvhz3hewjs4w0wggowgw8040gww40s','ZCFr06sTTLfhTybrBlIyvF9WwP15iNEMOMRpK6MS7Ymr/DcJ3wdy2BC5B5iU3/3KbQTZwrAvx6TNeqSjOUm/vg==',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'Katniss Everdeen','f',NULL,'','District 12 OÜ','2015-04-12 18:42:27','2015-04-12 18:42:27'),
  (4,'Peeta','peeta','Peeta.Mellark@example.com','peeta.mellark@example.com',1,'rohtrvz2ufk8gsss0cg0k8s800skcwk','LvvsUSC1sARiNrmIxuVeihK6AGwDW+OIFYX/kRBzO/qwVi49hz53wa6nQHAwXjmVGAYkV03LjAuiM0ho0TVLeQ==',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'Peeta Mellark','m',NULL,'','District 12 OÜ','2015-04-12 18:42:27','2015-04-12 18:42:27');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы user_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_group`;

CREATE TABLE `user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_8F02BF9DA76ED395` (`user_id`),
  KEY `IDX_8F02BF9DFE54D947` (`group_id`),
  CONSTRAINT `FK_8F02BF9DFE54D947` FOREIGN KEY (`group_id`) REFERENCES `group_role` (`id`),
  CONSTRAINT `FK_8F02BF9DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;

INSERT INTO `user_group` (`user_id`, `group_id`)
VALUES
  (1,1),
  (2,1),
  (3,2),
  (4,2);

/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
