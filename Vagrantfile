#If your Vagrant version is lower than 1.5, you can still use this provisioning
#by commenting or removing the line below and providing the config.vm.box_url parameter,
#if it's not already defined in this Vagrantfile. Keep in mind that you won't be able
#to use the Vagrant Cloud and other newer Vagrant features.
Vagrant.require_version ">= 1.5"

# Check to determine whether we're on a windows or linux/os-x host,
# later on we use this to launch ansible in the supported way
# source: https://stackoverflow.com/questions/2108727/which-in-ruby-checking-if-program-exists-in-path-from-ruby
def which(cmd)
    exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
    ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
        exts.each { |ext|
            exe = File.join(path, "#{cmd}#{ext}")
            return exe if File.executable? exe
        }
    end
    return nil
end
Vagrant.configure("2") do |config|

    config.vm.box = "ubuntu/trusty64"

    config.vm.network :private_network, ip: "192.168.33.99"
    config.ssh.forward_agent = true

    config.vm.provider :virtualbox do |v|
        cpus = case RbConfig::CONFIG['host_os']
            when /darwin/ then `sysctl -n hw.ncpu`.to_i
            when /linux/ then `nproc`.to_i
            else 2
        end

        v.name = "pkk"
        v.customize [
            "modifyvm", :id,
            "--name", "pkk",
            "--memory", 1024,
            "--natdnshostresolver1", "on",
            "--cpus", cpus,
        ]
    end

    if which('ansible-playbook')
        config.vm.provision "ansible" do |ansible|
            ansible.playbook = "ansible/playbook.yml"
            ansible.inventory_path = "ansible/inventories/dev"
            ansible.limit = 'all'
            ansible.extra_vars = {
                private_interface: "192.168.33.99",
                hostname: "pkk"
            }
        end
    else
        config.vm.provision :shell, path: "ansible/windows.sh", args: ["pkk"]
    end

    config.vm.synced_folder "./www", "/var/www/pkk", type: "nfs"

    config.vm.provision "shell", inline: "service nginx restart", run: "always"
end
